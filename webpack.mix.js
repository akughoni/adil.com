const mix = require('laravel-mix');
const tailwindcss = require("tailwindcss");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('tailwind.config.js')]
    });

mix.scripts([
    'resources/js/jquery-3.4.1.js',
    'resources/js/alertify.js',
    'resources/js/jquery.validate.js',
    'resources/js/jquery-ui.js',
    'resources/js/jquery-redirect.js',
    'resources/js/main.js',
], 'public/js/all.js');

mix.styles([
    'resources/css/alertify.css',
    'resources/css/styles.css',
    'resources/css/theme.css',
    'resources/css/default.css',
], 'public/css/all.css');

mix.styles([
    'resources/css/alertify.css',
    'resources/css/adil.css',
], 'public/css/user.css');