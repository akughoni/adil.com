<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifiedUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->user->verify->id);
        // return $this->from('admin@ghoni.id')
        //             ->view('vendor.mail.verified');
        // ->with('user', $this->user);

        return $this->to($this->user->email, $this->user->fullname)
                    ->subject('Email Verication Adil.com')
                    ->view('vendor.mail.verified')
                    ->with('user', $this->user);
    }
}
