<?php

namespace App\Imports;

use App\Models\Lecture;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Helpers\Generate;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LectureImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        return new Lecture([
            'id' => Generate::id(),
            'image' => Generate::image(),
            'section_id' => $row['section_id'],
            'title' => $row['title'],
            'ordering' => $row['ordering'],
            'description' => $row['description'],
            'video_url' => $row['video_url'],
            'duration' => $row['duration'],
        ]);
    }
}
