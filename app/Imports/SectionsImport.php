<?php

namespace App\Imports;

use App\Models\Section;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Helpers\Generate;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SectionsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Section([
            'id' => Generate::id(),
            'course_id'=> $row['course_id'],
            'title' => $row['title'],
            'ordering' => $row['ordering'],
            'description' => $row['description'],
            'image' => Generate::image()
        ]);
    }
}
