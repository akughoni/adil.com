<?php

namespace App\Imports;

use App\Models\Course;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Helpers\Generate;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CoursesImport implements ToModel , WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Course([
            'id' => Generate::id(),
            'title' => $row['title'],
            'ordering' => $row['ordering'],
            'description' => $row['description'],
            'image' => Generate::image()
        ]);
    }
}
