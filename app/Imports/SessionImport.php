<?php

namespace App\Imports;

use App\Models\Session_question;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Helpers\Generate;

class SessionImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Session_question([
            'id' => Generate::id(),
            'section_id' => $row['section_id'],
            'lecture_id' => $row['lecture_id'],
            'question' => $row['question'],
            'answer_a' => $row['answer_a'],
            'answer_b' => $row['answer_b'],
            'answer_c' => $row['answer_c'],
            'answer_d' => $row['answer_d'],
            'right' => $row['right']
        ]);
    }
}
