<?php

namespace App\Imports;

use App\Models\PretestQuestion;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Helpers\Generate;

class PretestImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PretestQuestion([
            'id' => Generate::id(),
            'course_id' => $row['course_id'],
            'section_id' => $row['section_id'],
            'question' => $row['question'],
            'answer_a' => $row['answer_a'],
            'answer_b' => $row['answer_b'],
            'answer_c' => $row['answer_c'],
            'answer_d' => $row['answer_d'],
            'right' => $row['right']
        ]);
    }
}
