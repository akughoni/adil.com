<?php

namespace App\Listeners\Auth;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\UserRegistration;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifiedUser as MailVerifiedUser;

class SendMailVerify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserRegistration $event)
    {
        $user = $event->data;
        Mail::send(new MailVerifiedUser($user));
    }
}
