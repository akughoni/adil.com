<?php

namespace App\Listeners\HistorySection;

use App\Events\TakeSectionCourse;
use App\Helpers\Generate;
use App\Models\HistorySection;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class HistorySectionStore
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TakeSectionCourse $event)
    {
        $id = $event->data;
        $history = HistorySection::updateOrCreate(
            [
                'user_id' => Auth::user()->id, 
                'section_id' => $id 
            ],
            [
                'id' => Generate::id(), 
                'user_id' => Auth::user()->id, 
                'section_id' => $id
            ]
        );
        
    }
}
