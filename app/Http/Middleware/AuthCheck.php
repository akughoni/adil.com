<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = 'user')
    {

        if (!Auth::check()) {
            if($role == 'admin'){
                return redirect()->route('AdminLogin');
            }elseif($role == 'user'){
                return redirect()->route('auth.login');
            }
            // return route('AdminLogin');
        }else{
            $user = Auth::user()->role->role;
            if($user == $role){
                return $next($request);
            }
        }
        return abort(403, 'Kamu Dilarang Masuk sini');
        
    }
}
