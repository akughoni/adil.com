<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Verified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(!$user->verified){
            Auth::logout();;
            return redirect()->route('auth.login')->with('status', 'You need to confirm your email. Web have send you an activation code, please check your email.')
                                                ->with('color','red');
        }
        return $next($request);
    }
}
