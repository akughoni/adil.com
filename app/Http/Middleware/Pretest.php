<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Pretest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pretest = Auth::user()->pretest;
        if(!empty($pretest))
        {
            return redirect()->route('user.dashboard');
        }
        return $next($request);
    }
}
