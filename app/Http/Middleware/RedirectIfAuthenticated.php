<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $check = Auth::guard($guard)->check();

        // dd($check);
        if (Auth::guard($guard)->check()) {
            $user = Auth::user()->role->role;
            if($user == 'admin'){
                return redirect()->route('AdminUser');
            }elseif($user == 'user'){
                return redirect()->route('user.dashboard');
            }
        }

        return $next($request);
    }
}
