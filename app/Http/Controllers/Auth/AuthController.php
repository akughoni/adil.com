<?php

namespace App\Http\Controllers\Auth;

use App\Events\ResetPassword;
use App\Events\UserRegistration;
use App\Helpers\Generate;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Jobs\MailUserVerify;
use App\Jobs\MailResetPassword;
use App\Models\Role;
use App\Models\VerifiedUser;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function index()
    {
        return view('User.pages.Auth.login');
    }

    public function registration()
    {
        return view('User.pages.Auth.register');
    }

    public function postLogin(Request $request)
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->route('user.dashboard');
        }
        return redirect()->route('auth.login')->with('status', 'Oppes! You have entered invalid credentials')
                                            ->with('color', 'red')
                                            ->withInput(
                                                $request->except('password')
                                            );
    }

    public function postRegistration(Request $request)
    {
        request()->validate([
            'fullname' => 'required|unique:users',
            'nim' => 'required|unique:users|max:12',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'confirm' => 'required|same:password',
        ]);

        $data = $request->all();
        $this->create($data);

        return redirect()->route("auth.login")->with('status', "Resgitration Successfully, please verify your email!.")
                                              ->with('color', 'green');
    }

    public function create(array $data)
    {
        $id = Generate::id();
        $role = Role::where('role', 'user')->first();
        $user =  User::create([
            'id' => $id,
            'fullname' => $data['fullname'],
            'nim' => $data['nim'],
            'email' => $data['email'],
            'role_id' => $role->id,
            'password' => Hash::make($data['password']),
        ]);

        $verify = VerifiedUser::create([
            'id' => Generate::id(),
            'user_id' => $id,
            'token' => sha1(time())
        ]);
        $user = User::find($id);

        // MailUserVerify::dispatch($user);
        event(new UserRegistration($user));

        return $user;
    }

    public function verify($token)
    {
        $verify = VerifiedUser::where('token', $token)->first();

        if (!empty($verify)) {
            $user = User::where('id', $verify->user_id)->first();
            if (!$user->verified) {
                $verify->user->verified = 1;
                $verify->user->save();
                $status = 'Your e-mail is verified. You can now login.';
            } else {
                $status = 'Your e-mail is already verified. You can now login.';
            }
        } else {
            return redirect()->route('auth.login')->with('status', 'Sorry your email cannot be identified.')->with('color', 'red');
        }
        
        return redirect()->route('auth.login')->with('status', $status)->with('color', 'green');
    }

    public function logout()
    {
        Auth::logout();
        return Redirect('/');
    }

    public function resetPassword()
    {
        return view('User.pages.Auth.resetPassword');
    }

    public function sendResetMail(Request $req)
    {
        $data = User::where('email', $req->email)->first();
        if (empty($data)) {
            return redirect()->route('auth.resetPassword')->with('status', 'Sorry your email cannot be identified.')->with('color', 'red');
        }
        // MailResetPassword::dispatch($data);
        event(new ResetPassword($data));
        return redirect()->route('auth.resetPassword')->with('status', "Check your email!. We'll send your request to reset a password.")->with('color', 'green');
    }

    public function newPassword($id)
    {
        return view('User.pages.Auth.newPassword')->with('id', $id);
    }

    public function storeNewPassword(Request $req)
    {
        $req->validate([
            'password' => 'required|min:6',
            'confirm' => 'required|same:password',
        ]);

        $user = User::findOrFail($req->id);
        $user->password = Hash::make($req->password);
        $user->save();

        return redirect()->route('auth.login')->with('status', "Reset password successfully, please login with new password!.")->with('color', 'green');
        // dd($user);
    }

    public function resend()
    {
        return view('User.pages.Auth.resend');
    }

    public function resendStore(Request $req)
    {
        $req->validate([
            'email' => 'required',
        ]);

        $email = $req->email;

        $user = User::where('email', $email)->first();
        event(new UserRegistration($user));

        return redirect()->route("auth.resend")->with('status', 'Resend mail successfully, please check your email again!')
                                            ->with('color', 'green');
    }

    public function help()
    {
        return view('User.pages.Auth.help');
    }
}
