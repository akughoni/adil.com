<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Generate;
use App\Http\Controllers\Controller;
use App\Imports\SessionImport;
use App\Models\Lecture;
use App\Models\Section;
use App\Models\Session_question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req, Session_question $session)
    {
        $limit = 100;
        $key = (!empty($req->key)) ? $req->key : '';
        $data = $session->latest()->with('section')
                        ->orWhere('question', 'like', '%'.$key.'%')
                        ->orderBy('section_id', 'DESC')
                        ->simplePaginate($limit);
        // dd($data);
        if ($req->ajax()) {
            return view('Admin.pages.session.tbody')->with('data', $data)->render();
        }
        return view('Admin.pages.session.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Section::orderBy('ordering')->get();
        return view('Admin.pages.session.create')
                    ->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $req->validate([
            'section_id' => 'required',
            'lecture_id' => 'required',
            'question' => 'required',
            'answer_a' => 'required',
            'answer_b' => 'required',
            'answer_c' => 'required',
            'answer_d' => 'required',
            'right' => 'required',
        ]);
        $data = Session_question::insert([
            'id' => Generate::id(),
            'section_id' => $req->section_id,
            'lecture_id' => $req->lecture_id,
            'set_pretest' => $req->set_pretest,
            'question' => $req->question,
            'answer_a' => $req->answer_a,
            'answer_b' => $req->answer_b,
            'answer_c' => $req->answer_c,
            'answer_d' => $req->answer_d,
            'right' => $req->right
        ]);

        return redirect()->route('session-tests.index');
    }

    /**
     * Display the Sections Select by Course ID
     *
     * @param  \App\Models\Session_question  $session_question
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req, Session_question $session)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Session_question  $session_question
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Session_question $session)
    {
        $data = Section::orderBy('ordering')->get();
        // dd($data);
        $item = $session->findOrFail($id);
        return view('Admin.pages.session.update')->with('data', $data)
                                                ->with('item', $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Session_question  $session_question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id, Session_question $session_question)
    {
        $req->validate([
            'section_id' => 'required',
            'lecture_id' => 'required',
            'question' => 'required',
            'answer_a' => 'required',
            'answer_b' => 'required',
            'answer_c' => 'required',
            'answer_d' => 'required',
            'right' => 'required',
        ]);

        $data = $session_question->where('id', $id)->update([
            'section_id' => $req->section_id,
            'lecture_id' => $req->lecture_id,
            'set_pretest' => $req->set_pretest,
            'question' => $req->question,
            'answer_a' => $req->answer_a,
            'answer_b' => $req->answer_b,
            'answer_c' => $req->answer_c,
            'answer_d' => $req->answer_d,
            'right' => $req->right
        ]);

        return redirect()->route('session-tests.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Session_question  $session_question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req, Session_question $session_question)
    {
        if ($req->ajax()) {
            // // hapus data
            $session_question->where('id', $req->id)->delete();
        }
        $res = [
            'status' => 'success'
        ];

        echo json_encode($res);
    }

    public function multiDelete(Request $req)
    {
        if ($req->ajax()) {
            $id = $req->id;
            $data = Session_question::whereIn('id', $id);
            $status = $data->delete();
            if ($status) {
                $res = ['status' => 'success'];
            } else {
                $res = ['status' => 'cancel'];
            }
        }
        echo json_encode($res);
    }

    public function import(Request $req)
    {
        $req->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);

        $file = $req->file;
        $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/session/import/";
        $file->move($pathUpload, $fileName);
        $filePath = public_path($pathUpload.$fileName);
        Excel::import(new SessionImport, $filePath);
        File::delete($filePath);
        
        return redirect()->route('session-tests.index');
    }

    public function getLecture(Request $req)
    {
        $option = '';
        $id = $req->id;
        $lectures = Lecture::where('section_id', $id)->orderBy('ordering')->get();
        foreach ($lectures as $item) {
            $option .="<option value='". $item->id."'>".$item->title."</option>";
        }
        echo $option;
    }

    public function setPretest(Request $req)
    {
        if($req->ajax())
        {
            $question = Session_question::where('id', $req->id)->first();
            $question->set_pretest = !$question->set_pretest;
            $question->save();

            echo $question->set_pretest;
        }
    }
}
