<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function login()
    {
        return view('Admin.pages.auth.login');
    }
    public function loginProses(Request $req)
    {
        if(Auth::attempt(['email' => $req->email, 'password' => $req->password]))
        {
            return redirect()->route('AdminUser');
        }
        return redirect()->back();
                
    }

    public function logout()
    {
        dd('haii');
        Auth::logout();
        return Redirect('/');
        
    }
}
