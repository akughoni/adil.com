<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Generate;
use App\Models\Course;
use App\Models\Section;
use App\Imports\CoursesImport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $limit = 10;
        $key = "";
        if (!empty($req->key)) {
            $key = $req->key;
        }
        $data = Course::with('sections')
                        ->orWhere('title', 'like', '%'.$key.'%')
                        ->orderBy('ordering', 'ASC')
                        ->simplePaginate($limit);

        if ($req->ajax()) {
            return view('Admin.pages.courses.tbody')->with('data', $data)->render();
        }
        return view('Admin.pages.courses.index')->with('data', $data);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countCourse = Course::orderby('ordering', 'DESC')->latest()->first();
        if ($countCourse == null) {
            $countCourse = 0;
        } else {
            $countCourse = $countCourse->ordering;
        }
        return view('Admin.pages.courses.create')
                ->with('ordering', $countCourse);
    }
    /*
    public function getDataCourses(Request $req)
    {
        if ($req->ajax()) {
            $key = $req->get('query');
            $tableRow = '';
            $data = [];
            
            if (!empty($key)) {
                $data = DB::table('courses')
                ->select(DB::raw('courses.id as id, courses.title , COUNT(sections.course_id) as section, courses.ordering'))
                ->leftJoin('sections', 'courses.id', 'sections.course_id')
                ->where('courses.id', 'like', '%'.$key.'%')
                ->orWhere('courses.title', 'like', '%'.$key.'%')
                ->groupBy('id')
                ->orderBy('courses.ordering', 'ASC')
                ->get();
            // dd($data);
            } else {
                $data = DB::table('courses')
                ->select(DB::raw('courses.id , courses.title , COUNT(sections.course_id) as section, courses.ordering '))
                ->leftJoin('sections', 'courses.id', 'sections.course_id')
                ->groupBy('id')
                ->orderBy('courses.ordering', 'ASC')
                ->get();
            }

            $tableRow = view('Admin.pages.courses.tbody')->with('data', $data)->render();
            $response = array(
                'row' => $tableRow
            );
            
            echo json_encode($response);
        }
    }
    */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $req->validate([
            'title' => 'required|min:3',
            'ordering' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:2048'
        ]);

        $file = $req->image;
        $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/courses/";
        $file->move($pathUpload, $fileName);
        $url = $pathUpload.$fileName;

        $course = new Course();
        $course->id = Generate::id();
        $course->title = $req->title;
        $course->description = $req->description;
        $course->image = $url;
        $course->status = 1;
        $course->save();

        return redirect()->route('AdminCourses');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);

        return view('Admin.pages.courses.update')
                ->with('course', $course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $req->validate([
            'title' => 'required|min:3',
            'ordering' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png|max:2048'
        ]);
        
        $course = Course::find($req->id);
        $course->title = $req->title;
        $course->description = $req->description;

        if (!empty($req->image)) {
            if (!empty($course->image)) {
                File::delete($course->image);  //delete file
            }
            $file = $req->image;
            $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
            $pathUpload = "images/courses/";
            $url = $pathUpload.$fileName;
            $file->move($pathUpload, $fileName); //upload file
            $course->image = $url; //update data di DB
        }
        $course->save();

        return redirect()->route('AdminCourses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        // $gambar = Gambar::where('id',$id)->first();
        $response = [];
        if ($req->ajax()) {
            $id = $req->id;

            $course = Course::where('id', $id)->first();
            
            File::delete($course->image);
            // // hapus data
            Course::where('id', $id)->delete();
        }
        // dd('hai');
        $response = [
            'status' => 'success'
        ];

        echo json_encode($response);
    }

    public function import(Request $req)
    {
        $req->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);

        $file = $req->file;
        $fileName = "Courses-Import".date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/courses/import/";
        $file->move($pathUpload, $fileName);
        $filePath = public_path($pathUpload.$fileName);
        Excel::import(new CoursesImport, $filePath);
        File::delete($filePath);
        
        return redirect()->route('AdminCourses');
    }

    public function multiDelete(Request $req)
    {
        if ($req->ajax()) {
            $id = $req->id;
            $data = Course::whereIn('id', $id);
            $status = $data->delete();
            if ($status) {
                $res = ['status' => 'success'];
            } else {
                $res = ['status' => 'cancel'];
            }
        }
        echo json_encode($res);
    }

    public function active($id)
    {
        $course = Course::where('id', $id)->first();
        $course->status = !$course->status;
        $course->save();

        return redirect()->back();
    }
}
