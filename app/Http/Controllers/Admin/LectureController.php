<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Generate;
use App\Models\Lecture;
use App\Models\Section;
use App\Http\Controllers\Controller;
use App\Imports\LectureImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Pagination\Paginator;
use Maatwebsite\Excel\Facades\Excel;

class LectureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $limit = 0;
        $key = "";
        if (!empty($req->key)) {
            $key = $req->key;
        }

        $data = Lecture::latest()->whereHas(
            'section',
            function ($query) use ($key) {
                $query->where('title', 'like', '%'.$key.'%');
            }
        )
            ->whereHas('section.course', function ($query) {
                $query->where('status', 1);
            })
            ->with('section')
            ->Where('title', 'like', '%'.$key.'%')
            ->orderBy('ordering')
            ->simplePaginate($limit);

        if ($req->ajax()) {
            return view('Admin.pages.lecture.tbody')->with('data', $data)->render();
        }
        return view('Admin.pages.lecture.index')
                ->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Section::get();
        $countLecture = Lecture::orderby('ordering', 'DESC')->latest()->first();
        if ($countLecture == null) {
            $countLecture = 0;
        } else {
            $countLecture = $countLecture->ordering;
        }
        return view('Admin.pages.lecture.create')
                ->with('data', $data)
                ->with('count', $countLecture);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $req->validate([
            'title' => 'required|min:3',
            'section_id' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:2048',
            'video_url' => 'required',
            'duration' => 'required'
        ]);

        $file = $req->image;
        $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/lecture/";
        $file->move($pathUpload, $fileName);
        $url = $pathUpload.$fileName;

        $course = new Lecture();
        $course->id = Generate::id();
        $course->title = $req->title;
        $course->section_id = $req->section_id;
        $course->description = $req->description;
        $course->image = $url;
        $course->video_url = $req->video_url;
        $course->duration = $req->duration;
        $course->ordering = $req->ordering;
        $course->save();

        return redirect()->route('AdminLecture');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Lecture::find($id);
        $section = Section::get();

        return view('Admin.pages.lecture.update')
                ->with('data', $data)
                ->with('section', $section);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        // dd('asad');
        $req->validate([
            'title' => 'required|min:3',
            'section_id' => 'required',
            'description' => 'required',
            'video_url' => 'required',
            'duration' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png|max:2048',
        ]);
        
        $data = Lecture::find($req->id);

        $data->title = $req->title;
        $data->section_id = $req->section_id;
        $data->description = $req->description;
        $data->video_url = $req->video_url;
        $data->duration = $req->duration;

        if (!empty($req->image)) {
            if (!empty($data->image)) {
                File::delete($data->image);  //delete file
            }
            $file = $req->image;
            $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
            $pathUpload = "images/lecture/";
            $url = $pathUpload.$fileName;
            $file->move($pathUpload, $fileName); //upload file
            $data->image = $url; //update data di DB
        }
        $data->save();

        return redirect()->route('AdminLecture');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $res = [];
        if ($req->ajax()) {
            $id = $req->id;

            $data = Lecture::where('id', $id)->first();
            
            File::delete($data->image);
            // // hapus data
            Lecture::where('id', $id)->delete();
        }
        // dd('hai');
        $res = [
            'status' => 'success'
        ];

        echo json_encode($res);
    }

    public function multiDelete(Request $req)
    {
        if ($req->ajax()) {
            $id = $req->id;
            $data = Lecture::whereIn('id', $id);
            $status = $data->delete();
            if ($status) {
                $res = ['status' => 'success'];
            } else {
                $res = ['status' => 'cancel'];
            }
        }
        echo json_encode($res);
    }

    public function import(Request $req)
    {
        $req->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);

        $file = $req->file;
        $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/lecture/import/";
        $file->move($pathUpload, $fileName);
        $filePath = public_path($pathUpload.$fileName);
        Excel::import(new LectureImport, $filePath);
        File::delete($filePath);
        
        return redirect()->route('AdminLecture');
    }
}
