<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ScoreExport;
use App\Http\Controllers\Controller;
use App\Models\PretestResult;
use App\Models\Section;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\UserPretest;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AdminUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $limit = 10;
        $data = User::where('role_id', '=', 2)->simplePaginate($limit);
        if ($req->ajax()) {
            $data = User::where('role_id', '=', 2)
            ->Where('fullname', 'like', '%'.$req->key.'%')
            ->Where('email', 'like', '%'.$req->key.'%')
            ->simplePaginate($limit);

            return view('Admin.pages.user.tbody')->with('data', $data)->render();
        }
        return view('Admin.pages.user.index')->with('data', $data);
    }

    public function userReset(Request $req)
    {
        $id = $req->id;
        
        $user = User::where('id', $id)->first();
        $user->sectionRecomend()->delete();
        $user->lectureRecomend()->delete();
        $user->historySection()->delete();
        $user->pretestResult()->delete();
        $user->pretest()->delete();

        return json_encode(['message' => 'Reset Success!']);
    }

    public function show($id)
    {
        $user = User::where('id', $id)->first();
        $id_section_recomended = $user->sections->map(function ($item) {
            return $item->id;
        });
        $section_pass = Section::whereHas('course', function ($query) { // untuk cek status pada relasi course
            $query->where('status', 1);
        })->sectionPass($id_section_recomended)
                        ->get();

        // dd($section_pass);
        return view(
            'Admin.pages.user.show',
            [
            'data'=> $user,
            'section_pass' => $section_pass
        ]
        );
    }

    public function exportScore()
    {
        return Excel::download(new ScoreExport, 'score_result.xlsx');
    }
}
