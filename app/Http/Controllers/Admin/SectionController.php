<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Generate;
use App\Models\Course;
use App\Models\Section;
use App\Http\Controllers\Controller;
use App\Imports\SectionsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $limit = 10;
        $key = "";
        if (!empty($req->key)) {
            $key = $req->key;
        }
        $data = Section::latest()->with(['lectures','course'])
        ->where('title', 'like', '%'.$key.'%')
        ->whereHas(
            'course',
            function ($query) use ($key) {
                $query->where('status', 1);
            }
        )
        ->orderBy('ordering', 'ASC')
        ->simplePaginate($limit);
        
        // dd($data);
        if ($req->ajax()) {
            return view('Admin.pages.section.tbody')->with('data', $data)->render();
        }
        return view('Admin.pages.section.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::get();
        $countSection = Section::orderby('ordering', 'DESC')->latest()->first();
        if ($countSection == null) {
            $countSection = 0;
        } else {
            $countSection = $countSection->ordering;
        }
        return view('Admin.pages.section.create')
                ->with('courses', $courses)
                ->with('count', $countSection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $req->validate([
            'title' => 'required|min:3',
            'course_id' => 'required',
            'ordering' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:2048'
        ]);

        $file = $req->image;
        $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/sections/";
        $file->move($pathUpload, $fileName);
        $url = $pathUpload.$fileName;

        $course = new Section();
        $course->id = Generate::id();
        $course->title = $req->title;
        $course->course_id = $req->course_id;
        $course->ordering = $req->ordering;
        $course->description = $req->description;
        $course->image = $url;
        $course->save();

        return redirect()->route('AdminSections');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section = Section::find($id);
        $courses = Course::get();

        return view('Admin.pages.section.update')
                ->with('data', $section)
                ->with('courses', $courses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        $req->validate([
            'title' => 'required|min:3',
            'course_id' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png|max:2048'
        ]);
        
        $data = Section::find($req->id);
        $data->title = $req->title;
        $data->course_id = $req->course_id;
        $data->description = $req->description;

        if (!empty($req->image)) {
            if (!empty($data->image)) {
                File::delete($data->image);  //delete file
            }
            $file = $req->image;
            $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
            $pathUpload = "images/sections/";
            $url = $pathUpload.$fileName;
            $file->move($pathUpload, $fileName); //upload file
            $data->image = $url; //update data di DB
        }
        $data->save();

        return redirect()->route('AdminSections');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $res = [];
        if ($req->ajax()) {
            $id = $req->id;

            $data = Section::where('id', $id)->first();
            
            File::delete($data->image);
            // // hapus data
            Section::where('id', $id)->delete();
        }
        // dd('hai');
        $res = [
            'status' => 'success'
        ];

        echo json_encode($res);
    }

    public function import(Request $req)
    {
        $req->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);

        $file = $req->file;
        $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/sections/import/";
        $file->move($pathUpload, $fileName);
        $filePath = public_path($pathUpload.$fileName);
        Excel::import(new SectionsImport, $filePath);
        File::delete($filePath);
        
        return redirect()->route('AdminSections');
    }
    public function multiDelete(Request $req)
    {
        if ($req->ajax()) {
            $id = $req->id;
            $data = Section::whereIn('id', $id);
            $status = $data->delete();
            if ($status) {
                $res = ['status' => 'success'];
            } else {
                $res = ['status' => 'cancel'];
            }
        }
        echo json_encode($res);
    }
}
