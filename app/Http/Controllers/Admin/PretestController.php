<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Generate;
use App\Http\Controllers\Controller;
use App\Imports\PretestImport;
use App\Models\Course;
use App\Models\PretestQuestion;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class PretestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req, PretestQuestion $pretest)
    {
        $limit = 5;
        $key = "";
        if(!empty($req->key)){
            $key = $req->key;
        }

        $data = $pretest->latest()->whereHas('section', 
                        function($query) use ($key){
                            $query->where('title','like', '%'.$key.'%');
                        })
                        ->with('section')
                        ->orWhere('question','like', '%'.$key.'%')
                        ->orderBy('section_id', 'DESC')
                        ->simplePaginate($limit);
        if($req->ajax()){
            return view('Admin.pages.pretest.tbody')->with('data', $data)->render();
        }
        return view('Admin.pages.pretest.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Section::orderBy('ordering')->get();
        return view('Admin.pages.pretest.create')
                ->with('data', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $req->validate([
            'section_id' => 'required',
            'question' => 'required',
            'answer_a' => 'required',
            'answer_b' => 'required',
            'answer_c' => 'required',
            'answer_d' => 'required',
            'right' => 'required',
        ]);

        $data = PretestQuestion::insert([
            'id' => Generate::id(),
            'section_id' => $req->section_id,
            'question' => $req->question,
            'answer_a' => $req->answer_a,
            'answer_b' => $req->answer_b,
            'answer_c' => $req->answer_c,
            'answer_d' => $req->answer_d,
            'right' => $req->right
        ]);

        return redirect()->route('pre-tests.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Course::all();
        $item = PretestQuestion::findOrFail($id);
        return view('Admin.pages.pretest.update')
                    ->with('data', $data)
                    ->with('item', $item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $req->validate([
            'section_id' => 'required',
            'question' => 'required',
            'answer_a' => 'required',
            'answer_b' => 'required',
            'answer_c' => 'required',
            'answer_d' => 'required',
            'right' => 'required',
        ]);

        $data = PretestQuestion::where('id',$id)
        ->update([
            'section_id' => $req->section_id,
            'question' => $req->question,
            'answer_a' => $req->answer_a,
            'answer_b' => $req->answer_b,
            'answer_c' => $req->answer_c,
            'answer_d' => $req->answer_d,
            'right' => $req->right
        ]);

        return redirect()->route('pre-tests.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if($req->ajax()){
            // // hapus data
            PretestQuestion::where('id',$req->id)->delete();
        }
        $res = [
            'status' => 'success'
        ];

        echo json_encode($res);
    }

    public function multiDelete(Request $req)
    {
        if($req->ajax())
        {
            $id = $req->id;
            $data = PretestQuestion::whereIn('id',$id);
            $status = $data->delete();
            if($status){
                $res = ['status' => 'success'];
            }else{
                $res = ['status' => 'cancel'];
            }

        }
        echo json_encode($res);
    }

    public function import(Request $req)
    {
        $req->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);

        $file = $req->file;
        $fileName = date('YmdHis').".". $file->getClientOriginalExtension();
        $pathUpload = "images/pretest/import/";
        $file->move($pathUpload,$fileName);
        $filePath = public_path($pathUpload.$fileName);
        Excel::import(new PretestImport, $filePath);
        File::delete($filePath);
        
        return redirect()->route('pre-tests.index');
    }
}
