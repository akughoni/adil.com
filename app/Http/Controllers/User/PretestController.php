<?php

namespace App\Http\Controllers\User;

use App\Helpers\Generate;
use App\Http\Controllers\Controller;
use App\Models\PretestResult;
use App\Models\RecomendedSection;
use App\Models\Section;
use App\Models\Lecture;
use App\Models\LectureRecomended;
use App\Models\Session_question;
use App\Models\User;
use App\Models\UserPretest;
use App\Models\UserSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PretestController extends Controller
{
    public $batas_nilai = 70;
    public function __construct()
    {
        $this->middleware('pretest');
    }

    public function index(Request $req)
    {
        $page = $req->page;

        $data = Session_question::whereHas('section.course', function ($query) {
            $query->where('status', 1);
        })
        ->where('set_pretest', 1)
        ->paginate(1);

        $count = Session_question::whereHas('section.course', function ($query) {
            $query->where('status', 1);
        })
        ->where('set_pretest', 1)
        ->get();
        
        $count = $count->count();

        if ($req->ajax()) {
            return view('User.pages.pretest.question')
                ->with('data', $data)
                ->with('count', $count)
                ->with('page', $page)
                ->render();
        }
        return view('User.pages.pretest.index')
            ->with('data', $data)
            ->with('page', $page)
            ->with('count', $count);
    }

    public function landing()
    {
        $count = Session_question::where('set_pretest', 1)->get();
        $count = $count->count();
        return view('User.pages.pretest.landing')
                ->with('count', $count);
    }

    public function submit(Request $req)
    {
        $score = collect();
        if ($req->ajax()) {
            $score = $this->checkAnswer($req, $score);  // Call Method checkAnswer(Request,New Collection);
            $result_by_section = $score->mapToGroups(function ($item, $key) { //group score berdasarkan section_id
                return [$item['section_id'] => $item['status']];
            });
            
            $pretest_result = $this->countResult($result_by_section);
            $user_pretest_id = $this->userPretestStore();
            $this->pretestResultStore($pretest_result, $user_pretest_id);
            $recomend = $this->getRecomend();
            $this->setSectionRecomend($recomend['section']);
            $this->setLectureRecomended($recomend['lecture']);
            $this->userSessionStore($recomend['pass']);
            // return json_encode(['status' => 'success']);
            return view('User.pages.pretest.report')
            ->with('result', $score);
        }
    }

    public function pretestResultStore($pretest_result, $user_pretest_id)
    {
        $pretest_result->each(function ($score, $section_id) use ($user_pretest_id) {
            $data = new PretestResult();
            $data->id = Generate::id();
            $data->user_pretest_id = $user_pretest_id;
            $data->section_id = $section_id;
            $data->score = $score;
            $data->save();
        });
    }

    public function setSectionRecomend($section)
    {
        $section->each(function ($item) {
            $data = new RecomendedSection;
            $data->id = Generate::id();
            $data->user_id = Auth::user()->id;
            $data->section_id = $item->id;
            $data->save();
        });
    }

    public function setLectureRecomended($lecture)
    {
        $lecture->each(function ($item) {
            $data = new LectureRecomended;
            $data->id = Generate::id();
            $data->user_id = Auth::user()->id;
            $data->lecture_id = $item->id;
            $data->save();
        });
    }

    public function getRecomend()
    {
        $section_id = collect();
        $user_id = Auth::user()->id;

        $user = User::with('pretest.pretest_result')
                        ->where('id', $user_id)
                        ->first();

        $recomend = $user->pretest->pretest_result->where('score', '<=', $this->batas_nilai);
        $pass = $user->pretest->pretest_result->where('score', '>', $this->batas_nilai);
        $recomend->each(function ($item) use ($section_id) {
            $section_id->push($item->section_id);
        });
        $section_recomend = Section::whereIn('id', $section_id->toArray())->get();
        $lecture_recomend = Lecture::whereIn('section_id', $section_id->toArray())->get();
        $response = [
            'section' => $section_recomend,
            'lecture'=> $lecture_recomend,
            'pass' => $pass
        ];

        return $response;
    }

    public function checkAnswer($req, $score)
    {
        foreach ($req->data as $index => $item) {
            $question = Session_question::where('id', $item['id'])->first();
            if (strtoupper($question->right) == $item['value']) {
                $result = [
                    'id' => $question->course_id,
                    'section_id'=> $question->section_id,
                    'title' => $question->section->title,
                    'status' => 'benar'
                ];
            } else {
                $result = [
                    'id' => $question->course_id,
                    'section_id'=> $question->section_id,
                    'title' => $question->section->title,
                    'status' => 'salah'
                ];
            }

            $score->push($result);
        }
        return $score;
    }

    public function countResult($score)
    {
        return $score->map(function ($item) {
            $success = $item->filter(function ($item) {
                return $item == 'benar';
            });
            $nilai = ($success->count() / $item->count()) * 100;
            return $nilai;
        });
    }

    public function userPretestStore()
    {
        $user_pretest_id = Generate::id();
        $user_pretest = new UserPretest();
        $user_pretest->id = $user_pretest_id;
        $user_pretest->user_id = Auth::user()->id;
        $user_pretest->save();

        return $user_pretest_id;
    }

    public function userSessionStore($pass)
    {
        $pass->each(function ($item) {
            $section = Section::where('id', $item->section_id)->first();

            $user_session = new UserSession();
            $user_session->id = Generate::id();
            $user_session->user_id = Auth::user()->id;
            $user_session->fullname= Auth::user()->fullname;
            $user_session->section_title = $section->title;

            $user_session->score = 100;
            $user_session->save();
        });
    }
}
