<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Section;
use Illuminate\Support\Facades\Auth;
use App\Models\Course;
use App\Models\HistorySection;
use App\Models\VerifiedUser;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $id_section_recomended = $this->getIdSectionRecomended($user);
        $section_pass = Section::whereHas('course', function ($query) { // untuk cek status pada relasi course
            $query->where('status', 1);
        })->sectionPass($id_section_recomended)->get();
        
        $last_courses = $user->lastSections()->with('historySection')->get();

        $take_section = $user->HistorySection()->count();
        $recomend_count = $id_section_recomended->count();
        return view('User.pages.dashboard.index')
            ->with('recomended', $user->sections->sortBy('ordering')->take(5))
            ->with('recomend_count', $recomend_count)
            ->with('courses_take', $take_section)
            ->with('last_courses', $last_courses->take(5))
            ->with('pass_count', $section_pass);
    }
    /*
    Return Array Sections ID for user login
    */
    public function getIdSectionRecomended($data)
    {
        return $section_id = $data->sections->map(function ($item) {
            return $item->id;
        });
    }
}
