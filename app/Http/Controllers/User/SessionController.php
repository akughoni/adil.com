<?php

namespace App\Http\Controllers\User;

use App\Helpers\Generate;
use App\Http\Controllers\Controller;
use App\Models\LectureRecomended;
use App\Models\Session_question;
use App\Models\PretestResult;
use App\Models\Section;
use App\Models\SessionResult;
use App\Models\UserPretest;
use App\Models\UserSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    public $batasNilai = 60;
    public function __construct()
    {
        // $this->middleware('pretest');
    }

    public function index($section_id, Request $req)
    {
        $page = $req->page;
        $lecture = Auth::user()->lectures()->where('lectures.section_id', $section_id)->get();
        $lecture = $lecture->map(function ($item) {
            return $item->id;
        });
        $data = Session_question::whereIn('lecture_id', $lecture->ToArray())->paginate(1);
        $count = Session_question::whereIn('lecture_id', $lecture->ToArray())->count();

        if ($req->ajax()) {
            return view('User.pages.session.question')
                ->with('data', $data)
                ->with('count', $count)
                ->with('page', $page)
                ->with('section_id', $section_id)
                ->render();
        }
        return view('User.pages.session.index')
            ->with('data', $data)
            ->with('page', $page)
            ->with('count', $count)
            ->with('section_id', $section_id);
    }

    public function landing()
    {
        $count = Session_question::get();
        $count = $count->count();
        return view('User.pages.session.landing')
                ->with('count', $count);
    }

    public function submit(Request $req)
    {
        $user = Auth::user();
        $result = collect();
        if ($req->ajax()) {
            $result = $this->checkAnswer($req, $result);
            $lecture_pass = $this->countResult($result);
            $lecture_fail = $this->countFailed($result);
            $score = $lecture_pass->count() / $result->count() * 100;
            $this->sessionResultStore($req->section_id, $score);

            if ($lecture_pass->count() == $result->count()) {
                $user->sectionRecomend()->where('section_id', $req->section_id)->delete();
                $this->userSessionStore($req->section_id);
            }

            $this->resetLecture($lecture_fail, $req->section_id);

            return view('User.pages.session.report')
            ->with('result', $result)
            ->with('section', $req->section_id);
        }
    }

    public function resetLecture($id, $section_id)
    {
        $user = Auth::user();
        $lecture = $user->lectureRecomend()->with('lecture')->whereHas('lecture', function ($query) use ($section_id) {
            $query->where('section_id', $section_id);
        })->delete();

        $id = $id->map(function ($item) {
            return $item['id'];
        });
        $id->each(function ($item) use ($user) {
            $data = new LectureRecomended();
            $data->id = Generate::id();
            $data->user_id = $user->id;
            $data->lecture_id = $item;
            $data->taken = 0;
            $data->save();
        });
    }

    public function checkAnswer($req, $score)
    {
        foreach ($req->data as $index => $item) {
            $question = Session_question::where('id', $item['id'])->with('lecture')->first();
            if (strtoupper($question->right) == $item['value']) {
                $result = [
                    'id' => $question->lecture_id,
                    'title' => $question->lecture->title,
                    'status' => 'benar'
                ];
            } else {
                $result = [
                    'id' => $question->lecture_id,
                    'title' => $question->lecture->title,
                    'status' => 'salah'
                ];
            }
            $score->push($result);
        }
        return $score;
    }

    public function countResult($score)
    {
        $success = $score->filter(function ($item) {
            return $item['status'] == 'benar';
        });
        return $success;
    }

    public function countFailed($score)
    {
        $failed = $score->filter(function ($item) {
            return $item['status'] == 'salah';
        });
        return $failed;
    }

    public function sessionResultStore($section_id, $score)
    {
        $session_result_id = Generate::id();
        $session_result = new SessionResult();
        $session_result->id = $session_result_id;
        $session_result->user_id = Auth::user()->id;
        $session_result->section_id = $section_id;
        $session_result->score = $score;
        $session_result->save();

        return $session_result;
    }

    public function userSessionStore($section_id)
    {
        $result = SessionResult::where([
            ['user_id', Auth::user()->id],
            ['section_id', $section_id]
        ])->get();
        
        $result = $result->map(function($item){
            return $item->score;
        });
        $section = Section::where('id', $section_id)->first();

        $user_session = new UserSession();
        $user_session->id = Generate::id();
        $user_session->user_id = Auth::user()->id;
        $user_session->fullname= Auth::user()->fullname;
        $user_session->section_title = $section->title;
        $user_session->score = $result->avg();
        $user_session->save();
        
    }

    public function pretest_result_store($pretest_result, $user_pretest_id)
    {
        $pretest_result->each(function ($score, $course_id) use ($user_pretest_id) {
            $data = new PretestResult();
            $data->id = Generate::id();
            $data->user_pretest_id = $user_pretest_id;
            $data->course_id = $course_id;
            $data->score = $score;
            $data->save();
        });
    }

    public function report()
    {
        return view('User.pages.session.report');
    }
}
