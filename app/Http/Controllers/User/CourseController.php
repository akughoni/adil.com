<?php

namespace App\Http\Controllers\User;

use App\Events\TakeSectionCourse;
use App\Http\Controllers\Controller;
use App\Models\Section;
use App\Models\Lecture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function index()
    {
        $user = Auth::user();
        $section = Auth::user()->sections()->orderBy('ordering')->simplePaginate(6);
        return view('User.pages.courses.index')
                ->with('sections', $section);
    }

    public function show($id)
    {
        $section = Section::find($id);
        $lecture = Auth::user()->lectures()->where('lectures.section_id', $id)->with('recomendedLecture')->orderBy('ordering')->get();
        
        event(new TakeSectionCourse($id));
        
        return view('User.pages.courses.show')
        ->with('lectures', $lecture)
        ->with('section', $section);
    }

    public function video($section_id)
    {
        $lectures = Auth::user()->lectures()->where('lectures.section_id', $section_id)->with('recomendedLecture')->orderBy('ordering')->get();
        $data = Auth::user()->lectures()->where('lectures.section_id', $section_id)->orderBy('ordering')->simplePaginate(1);

        $data->each(function($item){ //update taken lecture recomended to 1
            $item->recomendedLecture->update(['taken' => 1]);
            $item->recomendedLecture->save();
        });

        return view('User.pages.courses.video')
                ->with('data', $data)
                ->with('section_id', $section_id)
                ->with('list_lecture', $lectures);
    }
}
