<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        return view('User.pages.profile.index');
    }

    public function basic()
    {
        return view('User.pages.profile.basic')->render();
    }
    public function changePassword()
    {
        return view('User.pages.profile.changePassword')->render();
    }

    public function updatePassword(Request $req)
    {
        $req->validate([
            'old' => 'required',
            'new' => 'required|min:6',
            'confirm' => 'required|same:new'
        ]);

        $user = User::find(Auth::user()->id);

        // dd($req);

        if (Hash::check($req->old, $user->password)) {
            // dd('pass bener');
            $new_password = Hash::make($req->new);
            $user->password = $new_password;
            $user->save();

            Auth::logout();
            return redirect()->route("auth.login")->with('status', 'Change Your Password Successfully, please relogin with new password')
                                            ->with('color', 'green');
        }

        return redirect()->route('profile.index')->with('status', 'Your Old Password Is Wrong!')
        ->with('color', 'red');
    }

    public function store(Request $req)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        $user->fullname = $req->fullname;
        $user->nim = $req->nim;
        $user->email = $req->email;
        $user->save();

        return redirect()->route('profile.index')->with('status', 'Update your profile is Success!')
        ->with('color', 'green');
    }
}
