<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Section;
use App\Models\Lecture;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AllCourseController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function index()
    {
        $section = Section::whereHas('course', function ($query) {
            $query->where('status', 1);
        })->orderBy('ordering', 'ASC')->get();
        return view('User.pages.allCourses.index')
                ->with('sections', $section);
    }

    public function show($id)
    {
        $data = Lecture::where('section_id', $id)->with('section')->orderBy('ordering')->get();
        $section = Section::find($id);

        return view('User.pages.allCourses.show')
                    ->with('data', $data)
                    ->with('section', $section);
    }

    public function video($section_id)
    {
        $data = Lecture::where('section_id', $section_id)->orderBy('ordering')->simplePaginate(1);
        $list = Lecture::where('section_id', $section_id)->orderBy('ordering')->get();
        
        return view('User.pages.allCourses.video')
                ->with('data', $data)
                ->with('section_id', $section_id)
                ->with('list_lecture', $list);
    }
}
