<?php
namespace App\Helpers;
use Illuminate\Support\Str;

class Generate {
  public static function id(){
    $id =  date('ymd').Str::random(4). date('s');
    return strtoupper($id);
  }

  public static function image(){
    return 'images/default.jpg';
  }
}