<?php

namespace App\Exports;

use App\Models\UserSession;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ScoreExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return UserSession::orderBy('fullname')->orderBy('section_title')->get();
    }

    public function headings(): array
    {
        return [
            'Nama Lengkap',
            'Judul Course',
            'Score',
            'Tanggal'
        ];
    }

    public function map($score): array
    {
        return [
            $score->fullname,
            $score->section_title,
            $score->score,
            $score->created_at->format('d-m-Y')
        ];
    }

}
