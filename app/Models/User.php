<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $primaryKey = 'nim';

    protected $fillable = [
        'id','fullname','nim', 'email', 'password', 'role_id'
    ];
    protected $keyType = 'string';

    public function role(){
        return $this->belongsTo('App\Models\Role');
    }

    public function verify(){
        return $this->hasOne('App\Models\VerifiedUser','user_id','id');
    }

    public function pretest()
    {
        return $this->hasOne('App\Models\UserPretest','user_id','id');
    }

    public function sections()
    {
        return $this->belongsToMany('App\Models\Section','recomended_sections');
    }

    public function lectures()
    {
        return $this->belongsToMany('App\Models\Lecture','lecture_recomendeds');
    }

    public function sectionRecomend()
    {
        return $this->hasMany('App\Models\RecomendedSection');
    }

    public function lectureRecomend()
    {
        return $this->hasMany('App\Models\LectureRecomended');
    }

    public function historySection()
    {
        return $this->hasMany('App\Models\HistorySection');
    }

    public function lastSections()
    {
        return $this->belongsToMany('App\Models\Section','history_sections');
    }

    public function pretestResult()
    {
        return $this->hasManyThrough('App\Models\PretestResult', 'App\Models\UserPretest', 'user_id', 'user_pretest_id','id','id');
        // return $this->hasManyThrough('App\Models\PretestResult', 'App\Models\UserPretest', 'user_id (FK on middle table)', 'user_pretest_id (FK on las table)','local key on user','local key on user_pretest');

    }
}
