<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
  protected $keyType = 'string';
  protected $fillable = ['id','title', 'section_id', 'description' ,'image', 'video_url', 'duration','ordering'];
  
  public function section()
  {
    return $this->belongsTo('App\Models\Section');
  }

  public function user()
  {
    return $this->belongsToMany('App\Models\User','lecture_recomendeds');
  }

  public function recomendedLecture()
  {
    return $this->hasOne('App\Models\LectureRecomended'); // alur User (memiliki) lecture Recomendasi, Lecture Rekomendasi (punya 1) lecture
  }
}
