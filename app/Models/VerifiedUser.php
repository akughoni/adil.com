<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifiedUser extends Model
{
    protected $fillable = ['id', 'user_id', 'token'];

    
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
