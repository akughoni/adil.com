<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PretestQuestion extends Model
{
    protected $keyType = 'string';
    protected $table = 'pretest_questions';
    protected $fillable = ['id', 'course_id','section_id', 'question', 'answer_a','answer_b','answer_c','answer_d','right'];

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function section()
    {
        return $this->belongsTo('App\Models\Section');
    }

    public function setRightAttribute($value)
    {
        $this->attributes['right'] = strtoupper($value);
    }

}
