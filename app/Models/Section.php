<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Section extends Model
{
    protected $keyType = 'string';
    protected $fillable = ['id','course_id','title', 'description','ordering','image'];
  
    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }
    public function pretestQuestions()
    {
      return $this->hasMany('App\Models\PretestQuestion');
    }
    public function session_question()
    {
        return $this->hasMany('App\Models\Session_question');
    }
    public function lectures()
    {
        return $this->hasMany('App\Models\Lecture');
    }
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'recomended_sections');
    }
    public function scopeSectionPass($query, $id)
    {
        return $query->whereNotIn('id', $id);
    }
    public function history()
    {
        return $this->belongsToMany('App\Models\User', 'history_sections');
    }
    // get Section History from user -> Section ->section_history (karena itu has One)
    public function historySection()
    {
        return $this->hasOne('App\Models\HistorySection');
    }
}
