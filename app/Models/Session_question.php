<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session_question extends Model
{
    protected $table = 'session_questions';
    protected $keyType = 'string';
    protected $fillable = ['id', 'section_id','lecture_id','set_pretest','question', 'answer_a','answer_b','answer_c','answer_d','right'];

    public function section()
    {
        return $this->belongsTo('App\Models\Section');
    }
    public function lecture()
    {
        return $this->belongsTo('App\Models\Lecture');
    }
}
