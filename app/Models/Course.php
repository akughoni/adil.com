<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ['id','title','ordering','description','image'];
    protected $keyType = 'string';

    public function sections()
    {
      return $this->hasMany('App\Models\Section');
    }

    public function pretest(){
      return $this->hasMany('App\Models\Pretest_question');
    }

    public function scopeTitle($query, $key)
    {
      return $query->where('title', 'like', '%'. $key. '%');
    }

    public function questions()
    {
      return $this->hasMany('App\Models\PretestQuestion');
    }
}
