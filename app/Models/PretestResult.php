<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PretestResult extends Model
{
    protected $keyType = 'string';
    protected $fillable = [
        'id', 'user_pretest_id', 'course_id', 'score'
    ];
    
    public function user_pretest()
    {
        return $this->belongsTo('App\Models\UserPretest');
    }
}
