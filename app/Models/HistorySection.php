<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistorySection extends Model
{
    protected $guarded =[];
    

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function Section()
    {
        return $this->belongsTo('App\Models\Section');
    }
}
