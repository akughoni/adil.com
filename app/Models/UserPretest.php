<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPretest extends Model
{
    protected $keyType = 'string';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function pretest_result()
    {
        return $this->hasMany('App\Models\PretestResult');
    }
}
