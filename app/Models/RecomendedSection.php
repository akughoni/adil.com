<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecomendedSection extends Model
{
    protected $keyType = "String";
    protected $fillable = ['id', 'user_id', 'section_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
