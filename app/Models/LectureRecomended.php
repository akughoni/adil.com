<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LectureRecomended extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function lecture()
    {
        return $this->belongsTo('App\Models\Lecture');
    }
}
