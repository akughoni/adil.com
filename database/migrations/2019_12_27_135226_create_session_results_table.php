<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_results', function (Blueprint $table) {
            $table->string('id',12)->primary();
            $table->string('user_session_id',12)->nullable()->index();
            $table->string('section_id',12)->nullable()->index();
            $table->decimal('score');
            $table->timestamps();

            $table->foreign('user_session_id')->references('id')->on('user_sessions')->onDelete('set null');
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_results');
    }
}
