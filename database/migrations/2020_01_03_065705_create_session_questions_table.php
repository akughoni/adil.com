<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_questions', function (Blueprint $table) {
            $table->string('id', 12)->primary();
            $table->string('section_id', 12)->index()->nullable();
            $table->string('lecture_id', 12)->index()->nullable();
            $table->tinyInteger('set_pretest')->index()->default(0)->nullable();
            $table->text('question');
            $table->text('answer_a');
            $table->text('answer_b');
            $table->text('answer_c');
            $table->text('answer_d');
            $table->char('right');
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('sections')->onDelete('set null');
            $table->foreign('lecture_id')->references('id')->on('lectures')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_questions');
    }
}
