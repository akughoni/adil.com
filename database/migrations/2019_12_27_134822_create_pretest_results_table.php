<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePretestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pretest_results', function (Blueprint $table) {
            $table->string('id',12)->primary();
            $table->string('user_pretest_id',12)->nullable()->index();
            $table->string('section_id',12)->nullable()->index();
            $table->decimal('score');
            $table->timestamps();

            $table->foreign('user_pretest_id')->references('id')->on('user_pretests')->onDelete('set null');
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pretest_results');
    }
}
