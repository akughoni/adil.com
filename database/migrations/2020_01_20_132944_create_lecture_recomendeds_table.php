<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLectureRecomendedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecture_recomendeds', function (Blueprint $table) {
            $table->string('id', 12)->primary();
            $table->string('user_id', 12)->nullable()->index();
            $table->string('lecture_id', 12)->nullable()->index();
            $table->tinyInteger('taken')->default('0');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('lecture_id')->references('id')->on('lectures')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecture_recomendeds');
    }
}
