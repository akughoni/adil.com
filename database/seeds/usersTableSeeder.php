<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\Generate;
use phpDocumentor\Reflection\DocBlock\Tags\Generic;

class usersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => Generate::id(),
            'nim' => '11111111',
            'fullname' => 'Admin',
            'email' => 'asd@asd.com',
            'password' => bcrypt('asd'),
            'role_id' => '1'
        ]);
        DB::table('users')->insert([
            'id' => Generate::id(),
            'nim' => '160533611461',
            'fullname' => 'Ahmad Yunus Afghoni',
            'email' => 'akughoni@gmail.com',
            'password' => bcrypt('asd'),
            'role_id' => '2'
        ]);
    }
}
