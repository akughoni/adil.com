<!DOCTYPE html>
<html lang="en">
<head>
  @include('User.includes.head')
  <title>@yield('title')</title>
</head>

<body class="bg-white-2 h-full">
  @include('User.includes.navbar')
  <div class="w-full absolute @yield('h-bg','h-bg20rem') bg-green-purple-down mt-16 sm:mt-20 z-0">
  </div>
  @yield('content')
  @include('User.includes.footer')
  @yield('js')
</body>
</html>