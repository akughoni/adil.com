<div class="w-full lg:w-4/12 h-full mt-10 lg:mt-0">
  <h1 class="font-semibold font-lato text-lg text-gray-800">Nomer Soal</h1>
  <div class="pagination w-full h-auto px-0 sm:px-6 py-3 flex flex-wrap mt-2 bg-gray-100 justify-center items-center">
    {{ $data->onEachSide($count)->links('vendor.pagination.numberPretest') }}
  </div>
  <div class="flex flex-row mt-3 justify-center">
    <button id="submit" class="mx-2 px-8 py-2 bg-color2 hover:text-white-1 hover:bg-color2hover flex items-center text-white-2 rounded">
      Selesai
    </button>
    <button id="batal" class="mx-2 px-8 py-2 bg-white-5 flex items-center text-color2 rounded hover:bg-white-2">
      Batal?
    </button>
  </div>
</div>
<div id="question" class="lg:ml-10 w-full lg:w-8/12 h-auto lg:h-rem28 overflow-y-auto relative">
  {{-- <h1 class="font-semibold font-lato text-lg text-gray-800">Pertanyaan</h1> --}}
  @foreach ($data as $key => $item)
    <div id="{{ $item->id }}" number="{{ $page ?? 1 }}" class="sm:mt-3 text-sm sm:text-md text-gray-800 font-sans">
      {!! $item->question !!}
    </div>
    <div class="mt-3 flex flex-col text-gray-800">
      <div class="flex flex-row items-center my-1">  
        <input id="A" type="radio" name="{{ $item->id }}" value="A" class="answer mx-2 my-3 form-radio text-color2">
        <p class="px-2">A.</p>
        <label for="A" class="text-sm sm:text-md font-sans ">{!! $item->answer_a !!}</label>
      </div>
      <div class="flex flex-row items-center my-1">  
        <input id="B" type="radio" name="{{ $item->id }}" value="B" class="answer mx-2 my-3 form-radio text-color2">
        <p class="px-2">B. </p>
        <label for="B" class="text-sm sm:text-md font-sans ">{!! $item->answer_b !!}</label>
      </div>
      <div class="flex flex-row items-center my-1">  
        <input id="C" type="radio" name="{{ $item->id }}" value="C" class="answer mx-2 my-3 form-radio text-color2">
        <p class="px-2">C. </p>
        <label for="C" class="text-sm sm:text-md font-sans ">{!! $item->answer_c !!}</label>
      </div>
      <div class="flex flex-row items-center my-1">  
        <input id="D" type="radio" name="{{ $item->id }}" value="D" class="answer mx-2 my-3 form-radio text-color2">
        <p class="px-2">D. </p>
        <label for="D" class="text-sm sm:text-md font-sans ">{!! $item->answer_d !!}</label>
      </div>
    </div>
    @endforeach
    {{-- {{ $data->links() }} --}}
    <div class="flex flex-row mt-10">
      {{ $data->onEachSide(24)->links('vendor.pagination.pretest') }}
    </div>
</div>