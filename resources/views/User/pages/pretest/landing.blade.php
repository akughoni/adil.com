@extends('User.layout.default')
@section('title', 'Pre-Test')

@section('content')
  <div id="app" class="flex relative justify-center z-20 h-auto ">
    <div class="flex flex-col items-center relative h-auto w-10/12 mt-20">
      <div class=" px-2 sm:px-20 mt-10 text-center">
        <h1 class="text-white-1 font-lato text-xl sm:text-4xl">Bacalah Petunjuk Umum Berikut!</h1>
        <p class="text-white-2 text-md"></p>
      </div>
      <div id="question" class="flex mt-4 bg-white-1 w-full sm:w-10/12 h-auto shadow-md relative px-8 py-6 sm:px-16 sm:py-10 justify-center">
        <div class="w-full sm:w-10/12 flex flex-col justify-center">
          <div class="md:px-10 text-sm">
            <ol type="1" class="list-decimal text-gray-800 font-roboto leading-relaxed text-sm sm:text-md">
              <li>Berdo’a sebelum melaksanakan pretest.</li>
              <li>Kerjakan dengan maksimal, tidak ada batasan waktu pengerjaan.</li>
              <li>Bacalah dengan teliti soal dan jawaban yang tersedia.</li>
              <li>Periksa semua jawaban sebelum anda melakukan submit</li>
              <li>Kerjakan sesuai dengan kemampuannmu, tidak diperkenankan mencontek atau melakukan pencarian jawaban di internet.</li>
              <li>Laporkan pada admin jika terdapat permasalahan dalam pengerjaan pre-test.</li>
            </ol>
          </div>
          <div class="md:px-10 flex flex-col justify-center mt-5 text-center">
            <p class="text-md text-gray-800 font-roboto sm:font-semibold">Klik dibawah ini untuk memulai pretest!</p>
            <a href="{{ route('user.pretest') }}" class="mx-2 text-center px-8 py-2 bg-color2 rounded text-white-1 font-roboto hover:bg-color2hover mt-3">
              <span class="">
                Mulai Sekarang
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
  @include('User.pages.pretest.js')
@endsection