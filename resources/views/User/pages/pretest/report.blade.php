  <div class="w-full min-h-full flex flex-col items-center">
    <div class="md:px-10 flex flex-row justify-center my-5 text-center">
      <a href="{{ route('user.dashboard') }} ">
        <span
          class="text-center px-8 py-2 bg-color2 rounded text-white-1 font-roboto hover:bg-color2hover mx-1">
          Dashboard
        </span>
      </a>
    </div>
    <div class="md:px-10 text-sm w-3/4">
      @foreach($result as $key => $item)
      <div class="px-5 py-3 @if($item['status'] == 'benar') bg-green-300 @else bg-red-300 @endif rounded my-1 flex flex-row">
        <p class="@if($item['status'] == 'benar') text-green-900 @else text-red-900 @endif w-10/12">
          {{ $key+1 }}. {{ $item['title'] }} 
        </p>
        <span class="w-auto uppercase font-bold @if($item['status'] == 'benar') text-green-900 @else text-red-900 @endif">{{ $item["status"] }}</span>
      </div>
      @endforeach
    </div>
  </div>