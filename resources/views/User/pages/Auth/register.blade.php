<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adil.css') }}" rel="stylesheet">
    <title>Adil | Register Page</title>
</head>

<body class="">
    <nav class="flex px-10 md:mx-0 md:flex-row absolute z-10 md:px-32 py-5 w-full justify-center items-center">
        <div class="w-1/4 md:w-full">
          <a href="{{ route('landing') }}"><h1 class="font-lobster text-white-2 text-5xl text-center md:text-left">Adil.</h1></a>
        </div>
    </nav>
    <div class="flex flex-col md:flex-row md:h-screen z-0 items-center">
        {{-- image page --}}
        <div class="w-full md:w-4/6 ">
            <img src="{{ asset('images/register.jpg') }}" class="h-64 w-full md:h-screen object-cover" alt="" srcset="">
            <div class="absolute h-64 w-full bg-green-purple opacity-88 top-0 left-0 md:h-screen md:w-4/6">
            </div>
            <div class="text-center md:text-right my-24 absolute w-full h-64 top-0 left-0 md:w-6/12 md:mx-32 md:my-40">
                {{-- <h1 class="font-playfairDisplay text-6xl text-white-1 font-bold tracking-wide leading-tight">Selamat Datang! Ayo Belajar Pemrograman Web</h1> --}}
                <h1 class="font-playfairDisplay text-xl md:text-5xl text-white-1 font-bold tracking-wide leading-tight">
                    Create your account to start</h1>
                <h1
                    class="font-playfairDisplay inline-block text-3xl md:text-5rem  text-white-1 font-bold tracking-wide leading-tight">
                    Learning Web</h1>
                <h1
                    class="font-playfairDisplay inline-block text-3xl md:text-6rem text-white-1 font-bold tracking-wide leading-tight">
                    Programming</h1>
                <p class="text-white-1 px-12 md:px-0 text-xs font-openSans md:text-lg">Media Pembelajaran Adaptive
                    dengan Model Dynamic Intellectual Learning (DIL) untuk Mata Kuliah Pemrograman Web</p>
            </div>
        </div>
        {{-- Login Page --}}
        <div class="w-full my-10 md:w-2/6">
            <div class="border-b-2 border-tosca w-4/6 mx-16">
                <h1 class="text-tosca text-3xl md:text-4xl font-bold font-roboto">Sign Up!</h1>
            </div>
            <form action="{{ route('auth.postRegistration') }}" method="post">
                @csrf
                <div class="flex flex-col w-4/6 mt-3 mx-16">
                    <label class="text-white-4 font-light" for="fullname">Fullname</label>
                    <input class="px-1 py-1 rounded border-2 border-white-5 text-gray-700" type="text" name="fullname" id="fullname" value="{{ old('fullname') }}">
                    @error('fullname')
                        <p class="text-xs text-red-500 font-openSans">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col w-4/6 mt-3 mx-16">
                    <label class="text-white-4 font-light" for="nim">NIM</label>
                    <input class="px-1 py-1 rounded border-2 border-white-5 text-gray-700" type="text" name="nim" id="nim" value="{{ old('nim') }}">
                    @error('nim')
                        <p class="text-xs text-red-500 font-openSans">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col w-4/6 mt-3 mx-16">
                    <label class="text-white-4 font-light" for="email">Email</label>
                    <input class="px-1 py-1 rounded border-2 border-white-5 text-gray-700" type="text" name="email" id="email" value="{{ old('email') }}">
                    @error('email')
                        <p class="text-xs text-red-500 font-openSans">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col w-4/6 mt-3 mx-16">
                    <label class="text-white-4 font-light" for="password">Password</label>
                    <input class="px-1 py-1 rounded border-2 border-white-5 text-gray-700" type="password" name="password"
                        id="password" value="{{ old('password') }}">
                    @error('password')
                        <p class="text-xs text-red-500 font-openSans">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col w-4/6 mt-3 mx-16">
                    <label class="text-white-4 font-light" for="confirm">Confirm Password</label>
                    <input class="px-1 py-1 rounded border-2 border-white-5 text-gray-700" type="password" name="confirm"
                        id="confirm">
                    @error('confirm')
                        <p class="text-xs text-red-500 font-openSans">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col md:flex-row w-4/6 mt-3 mx-16 items-center mt-5">
                    <button class="bg-color2 w-full py-2 rounded text-white-1" type="submit">Sign Up</button>
                    {{-- <a href="#" class="w-full text-center py-2 md:py-0 md:text-right md:w-3/6 text-color2">Forgot Password?</a> --}}
                </div>
                <div class="w-4/6 mt-3 mx-16 text-center md:text-left">
                    <p class="text-white-4">Sudah punya akun? <a href={{ route('auth.login') }} class="text-color2">Login disini</a></p>
                </div>
            </form>
        </div>
    </div>
</body>

</html>