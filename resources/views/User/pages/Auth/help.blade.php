<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adil.css') }}" rel="stylesheet">
    <title>Adil | Login Page</title>
</head>

<body class="">
    <nav class="flex px-10 md:mx-0 md:flex-row absolute z-10 md:px-32 py-5 w-full items-center">
      <div class="w-1/4 md:w-4/6">
          <a href="{{ route('landing') }}"><h1 class="font-lobster text-white-2 text-5xl text-center md:text-left">Adil.</h1></a>
      </div>
      <div class="mt-2 w-2/4 md:w-2/6 text-right hidden md:block">
          <a class="mx-5 text-white-1 py-2 md:text-tosca font-medium font-roboto text-lg" href="{{ route('landing','#about') }} ">Tentang</a>
          <a class="py-2 text-white-1 md:text-tosca font-medium font-roboto text-lg" href="{{ route('auth.help') }}">Bantuan</a>
      </div>
    </nav>
    <div class="flex flex-col md:flex-row md:h-screen z-0 items-start">
        {{-- image page --}}
        <div class="w-full md:w-4/6 ">
            <img src="{{ asset('images/login2.jpg') }}" class="h-64 w-full md:h-screen object-cover" alt="" srcset="">
            <div class="absolute h-64 w-full bg-green-purple opacity-88 top-0 left-0 md:h-screen md:w-4/6">
            </div>
            <div class="text-center md:text-left my-24 absolute w-full h-64 top-0 left-0 md:w-6/12 md:mx-32 md:my-40">
                {{-- <h1 class="font-playfairDisplay text-6xl text-white-1 font-bold tracking-wide leading-tight">Selamat Datang! Ayo Belajar Pemrograman Web</h1> --}}
                <h1
                    class="font-playfairDisplay text-2xl md:text-6xl text-white-1 font-bold tracking-wide leading-tight">
                    Welcome Back!</h1>
                <h1
                    class="font-playfairDisplay inline-block text-4xl md:text-6rem  text-white-1 font-bold tracking-wide leading-tight">
                    Get Started</h1>
                <h1
                    class="font-playfairDisplay inline-block text-4xl md:text-6rem text-white-1 font-bold tracking-wide leading-tight">
                    Learning</h1>
                <p class="text-white-1 px-16 md:px-0 text-xs font-openSans md:text-lg">Media Pembelajaran Adaptive
                    dengan Model Dynamic Intellectual Learning (DIL) untuk Mata Kuliah Pemrograman Web</p>
            </div>
        </div>
        {{-- Login Page --}}
        <div class="w-full my-10 md:my-32 md:w-2/6">
            @if(session('status'))
            <div class="mx-16 my-5 w-auto px-3 py-2 bg-{{ session('color') }}-200 text-{{ session('color') }}-900 text-sm rounded opacity-88">
                {{ session('status') }}
            </div>
            @endif
            <div class="border-b-2 border-tosca w-4/6 mx-16">
                <p class="text-tosca text-5xl font-bold font-lato">Bantuan</p>
            </div>
            <div class="mx-20 my-2 text-teal-900 font-lato text-lg">
                <ul class="list-disc">
                    <li class="my-2 hover:text-teal-800">
                        <a href="{{ route('auth.resetPassword') }}">Lupa password? </a>
                    </li>
                    <li class="my-2 hover:text-teal-800">
                        <a href="{{ route('auth.resend') }}">Tidak menerima email verifikasi? </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>

</html>