<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adil.css') }}" rel="stylesheet">
    <title>Adil | Login Page</title>
</head>

<body class="">
    <nav class="flex px-10 md:mx-0 md:flex-row absolute z-10 md:px-32 py-5 w-full items-center">
        <div class="w-1/4 md:w-4/6">
            <h1 class="font-lobster text-white-2 text-5xl text-center md:text-left">Adil.</h1>
        </div>
    </nav>
    <div class="flex flex-col md:flex-row md:h-screen z-0 items-center">
        {{-- image page --}}
        <div class="w-full md:w-4/6 ">
            <img src="{{ asset('images/login2.jpg') }}" class="h-64 w-full md:h-screen object-cover" alt="" srcset="">
            <div class="absolute h-64 w-full bg-green-purple opacity-88 top-0 left-0 md:h-screen md:w-4/6">
            </div>
            <div class="text-center md:text-left my-24 absolute w-full h-64 top-0 left-0 md:w-6/12 md:mx-32 md:my-40">
                {{-- <h1 class="font-playfairDisplay text-6xl text-white-1 font-bold tracking-wide leading-tight">Selamat Datang! Ayo Belajar Pemrograman Web</h1> --}}
                <h1
                    class="font-playfairDisplay text-2xl md:text-6xl text-white-1 font-bold tracking-wide leading-tight">
                    Welcome Back!</h1>
                <h1
                    class="font-playfairDisplay inline-block text-4xl md:text-6rem  text-white-1 font-bold tracking-wide leading-tight">
                    Get Started</h1>
                <h1
                    class="font-playfairDisplay inline-block text-4xl md:text-6rem text-white-1 font-bold tracking-wide leading-tight">
                    Learning</h1>
                <p class="text-white-1 px-16 md:px-0 text-xs font-openSans md:text-lg">Media Pembelajaran Adaptive
                    dengan Model Dynamic Intellectual Learning (DIL) untuk Mata Kuliah Pemrograman Web</p>
            </div>
        </div>
        {{-- Login Page --}}
        <div class="w-full my-10 md:my-0 md:w-2/6">
            @if(session('status'))
            <div class="mx-16 w-auto px-3 py-2 bg-{{ session('color') }}-200 text-{{ session('color') }}-900 text-sm rounded opacity-88">
                {{ session('status') }}
            </div>
            @endif
            <div class="border-b-2 border-tosca w-4/6 mx-16">
                <h1 class="text-tosca text-2xl font-bold font-roboto">Please insert a</h1>
                <h1 class="text-tosca text-4xl font-bold font-roboto">New Password!</h1>
            </div>
            <form action="{{ route('auth.store.newPassword') }}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{ $id }} ">
                <div class="flex flex-col w-4/6 mt-3 mx-16">
                    <label class="text-white-4 font-light" for="email">Your New Password</label>
                    <input class="px-1 py-1 text-gray-700 rounded border-2 border-white-5" type="password" name="password"
                        id="password" value="{{ old('password') }}">
                    @error('password')
                        <p class="text-xs text-red-500 font-openSans">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col w-4/6 mt-3 mx-16">
                    <label class="text-white-4 font-light" for="confirm">Confirm Your New Password</label>
                    <input class="px-1 py-1 rounded border-2 border-white-5" type="password" name="confirm"
                        id="confirm">
                        @error('confirm')
                        <p class="text-xs text-red-500 font-openSans">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col md:flex-row w-4/6 mt-3 mx-16 items-center mt-5">
                    <button class="bg-color2 w-full md:w-full py-2 rounded text-white-1 hover:text-white-5"
                        type="submit">Submit New Password</button>
                </div>
                <div class="w-4/6 mt-3 mx-16 text-center md:text-left">
                    <p class="text-white-4">Belum punya akun? <a href="{{ route('auth.registration') }}" class="text-color2">Daftar disini</a></p>
                </div>
            </form>
        </div>
    </div>
</body>

</html>