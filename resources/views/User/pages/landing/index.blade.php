<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adil.css') }}" rel="stylesheet">
    <link href="{{ asset('css/user.css') }}" rel="stylesheet">
    <title>Adil | Login Page</title>
</head>

<body class="">
    <nav class="flex px-5 sm:px-10 md:mx-0 md:flex-row absolute z-10 md:px-32 py-5 w-full items-center">
        <div class="w-1/4 lg:w-1/6 inline-flex items-center">
            <img src="{{ asset('images/logo-um.png') }}" alt="logo_um" srcset="" class="w-12 h-12">
            <div class="w-1 h-12 bg-white-2 mx-3"></div>
            <a href="{{ route('landing') }}">
                <h1 class="font-lobster text-white-2 text-5xl text-center md:text-left">Adil.</h1>
            </a>
        </div>
        <div class="mt-2 w-2/4 lg:w-5/6 text-right hidden lg:inline-block">
            <a class="mx-4 text-white-1 py-2 font-lato text-lg" href="#about">About</a>
            <a class="mx-4 text-white-1 py-2 font-lato text-lg" href="#feature">Features</a>
            <a class="mx-4 text-white-1 py-2 font-lato text-lg" href="#team">Team</a>
            <a class="mx-3 text-white-1 py-2 px-8 border-2 border-white-1 font-lato text-lg rounded-lg "
                href="{{ route('auth.registration') }}">Register</a>
            @if(Auth::check())
            <a class="mx-3 text-white-1 py-2 px-8 font-lato text-lg bg-tosca rounded-lg border-2 border-white-1"
                href="{{ route('user.dashboard') }}">Dashboard</a>
            @else
            <a class="mx-3 text-white-1 py-2 px-8 font-lato text-lg bg-tosca rounded-lg border-2 border-white-1"
                href="{{ route('auth.login') }}">Login</a>
            @endif
        </div>
        <div class="mt-2 w-full md:w-4/6 text-right block lg:hidden">
            <a class="mx-3 text-white-1 py-2 px-8 font-lato text-lg bg-tosca rounded-lg border-2 border-white-1"
                href="{{ route('auth.login') }}">Login</a>
        </div>
    </nav>
    {{-- Landing --}}
    <section>
        <img class="z-0 absolute h-rem32 sm:h-rem40 lg:h-rem40 xl:h-auto object-cover"
            src="{{ asset('images/landing/bg-2.svg') }} " alt="" srcset="">
        <img class="z-0 absolute h-rem32 sm:h-rem40 lg:h-rem40 xl:h-auto object-cover"
            src="{{ asset('images/landing/shapes-bg.svg') }} " alt="" srcset="">
        <div class="relative pt-32 sm:pt-40 sm:px-24 lg:px-32">
            <div class="flex flex-col lg:flex-row z-10 flex-row relative">
                <div class="lg:w-6/12 px-5 text-center sm:text-left">
                    <p class="font-lobster text-5xl adil text-white-2 leading-tight">Adil.</p>
                    <p class="font-openSans text-xl sm:text-3xl lg:text-5xl leading-tight text-white-2">
                        <strong>A</strong>daptive
                        <strong>D</strong>ynamic <strong>I</strong>ntellectual <strong>L</strong>earning</p>
                    <p class="text-sm sm:text-lg text-white-5 font-openSans">Media Pembelajaran Adaptive dengan Model
                        Dynamic Intellectual Learning (DIL) untuk Mata Kuliah Pemrograman Web</p>
                </div>
                <div class="w-full lg:w-6/12 px-10 sm:px-5 mt-8 sm:mt-0">
                    <img class="w-full" src="{{ asset('images/landing/video.svg') }} " alt="" srcset="">
                </div>
            </div>
        </div>
    </section>

    {{-- About --}}
    <div></div>
    <section id="about" class="lg:h-rem34">
        <div class="relative flex flex-col-reverse lg:flex-row sm:items-end sm:px-16 xl:px-32 my-8 sm:my-16">
            <div class="lg:w-1/2 flex justify-center z-10 mt-10 sm:mt-0">
                <img class="sm:w-8/12 lg:w-full lg:ob" src="{{ asset('images/landing/two_people.svg') }} " alt=""
                    srcset="">
            </div>
            <img class="z-0 absolute h-32 sm:h-rem28 md:h-rem32 lg:h-rem40 bottom-0 sm:top-0 right-0 xl:w-7/12"
                src="{{ asset('images/landing/bg-about.svg') }} " alt="" srcset="">
            <div
                class="flex flex-col z-10 items-center sm:items-end px-6 sm:px-0 sm:w-2/3 xl:w-1/2 md:pl-16 lg:pl-10 sm:pt-5 md:pt-10 xl:pt-16">
                <div class="border-b-4 border-color2 sm:border-white-5 my-5 md:w-4/12 rounded">
                    <h1 class="text-color2 sm:text-white-5 font-lato font-bold text-3xl lg:text-5xl text-right">About
                    </h1>
                </div>
                <p
                    class="text-center sm:text-right text-sm sm:text-md lg:text-lg text-color2 sm:text-white-5 leading-normal">
                    Peserta didik memiliki karakteristik yang berbeda-beda. Pembelajaran akan berjalan dengan maksimal
                    jika mampu untuk menyesuaikan dengan karakteristik peserta didik. Salah satu karakteristik peserta
                    didik yang dapat diseseuaikan dengan pembelajaran yaitu pengetahuan awal. Maka dalam penelitian
                    pengembangan ini dikembangkan sebuah media yang mampu menyesuaikan dengan kemampuan awal peserta
                    didik. Adil adalah sebuah web learning yang mampu merekomendasikan materi yang sesuai dengan
                    kemampuan pengguna, berdasarkan hasil pretest yang dilakukan oleh pengguna.
                </p>
            </div>
        </div>
    </section>
    {{-- Feature --}}
    <section id="feature" class="sm:h-rem34 sm:my-24">
        <div class="flex justify-center sm:px-32 mt-16">
            <p class="text-color2hover font-lato font-bold text-2xl sm:text-5xl border-color2 border-b-4 rounded">
                Features</p>
        </div>
        <div class="flex flex-col sm:flex-row z-10 sm:pl-16 pt-10 sm:pt-16">
            <div class="flex flex-col sm:w-1/3 mx-2 px-12 sm:px-2 text-center">
                <div class="">
                    <img class="h-64" src="{{ asset('images/landing/observasi.svg') }} " alt="" srcset="">
                </div>
                <h1 class="text-xl sm:text-2xl text-color2 font-lato font-bold">Pretest</h1>
                <p class="text-sm sm:text-md text-color2 font-openSans">Pretest digunakan untuk mengetahui tingkat
                    kemampuan awal pengguna.
                </p>
            </div>
            <div class="flex flex-col sm:w-1/3 mx-2 px-12 sm:px-2 text-center">
                <div>
                    <img class="h-64" src="{{ asset('images/landing/recomend.svg') }} " alt="" srcset="">
                </div>
                <h1 class="text-xl sm:text-2xl text-color2 font-lato font-bold">Recomended Courses</h1>
                <p class="text-sm sm:text-md text-color2 font-openSans">Sistem akan memberikan rekomendasi materi sesuai
                    dengan hasil
                    pretest.</p>
            </div>
            <div class="flex flex-col sm:w-1/3 mx-2 px-12 sm:px-2 text-center">
                <div>
                    <img class="h-64" src="{{ asset('images/landing/session.svg') }} " alt="" srcset="">
                </div>
                <h1 class="text-xl sm:text-2xl text-color2 font-lato font-bold">Session Test</h1>
                <p class="text-sm sm:text-md text-color2 font-openSans">Test untuk merekomendasikan ulang topik yang
                    belum kamu fahami.</p>
            </div>
        </div>
        </div>
        <div id="team"></div>
    </section>

    {{-- Team --}}
    <section class="h-auto sm:h-rem34 lg:px-32 mt-16">
        <div class="flex flex-row justify-center">
            <h1 class="text-color2hover font-lato font-bold text-2xl sm:text-5xl border-color2 border-b-4 rounded">Our
                Teams</h1>
        </div>
        <div class="flex flex-col sm:flex-row z-10 sm:pl-16 pt-5 sm:pt-16">
            <div class="flex flex-col sm:w-1/3 mx-2 px-10 sm:px-2 text-center items-center">
                <div
                    class="w-1/3 sm:w-2/3 flex justify-center items-start h-32 sm:h-rem16 rounded-full overflow-hidden">
                    <img class="object-cover w-full sm:w-auto sm:h-full" src="{{ asset('images/landing/p1.jpg') }} "
                        alt="" srcset="">
                </div>
                <h1 class="text-sm sm:text-xl text-color2 font-lato font-bold">Dr. H. Hakkun Elmunsyah, S.T., M.T.</h1>
                <p class="text-sm sm:text-md text-color2 font-openSans">Dosen Pembimbing 1</p>
            </div>
            <div class="flex flex-col sm:w-1/3 mx-2 px-10 sm:px-2 text-center items-center">
                <div class="w-1/3 sm:w-2/3 flex justify-center items-start h-32 sm:h-rem16 rounded-full overflow-hidden"">
                    <img class=" object-cover w-full sm:w-auto sm:h-full" src="{{ asset('images/landing/p2.jpg') }} "
                    alt="" srcset="">
                </div>
                <h1 class="text-sm sm:text-xl text-color2 font-lato font-bold">Heru Wahyu Herwanto, S.T., M.Kom.</h1>
                <p class="text-sm sm:text-md text-color2 font-openSans">Dosen Pembimbing 2</p>
            </div>
            <div class="flex flex-col sm:w-1/3 mx-2 px-10 sm:px-2 text-center items-center">
                <div class="w-1/3 sm:w-2/3 flex justify-center items-start h-32 sm:h-rem16 rounded-full overflow-hidden"">
                    <img class=" object-cover h-full" src="{{ asset('images/landing/p3.png') }} " alt="" srcset="">
                </div>
                <h1 class="text-sm sm:text-xl text-color2 font-lato font-bold">Ahmad Yunus Afghoni</h1>
                <p class="text-sm sm:text-md text-color2 font-openSans">Peneliti dan Pengembang</p>
            </div>
        </div>
    </section>
    <footer class="lg:-mt-20">
        <div class="relative">
            <img class="z-0 absolute object-cover h-rem28 sm:h-rem28 lg:h-auto bottom-0"
                src="{{ asset('images/landing/footer.svg') }} " alt="" srcset="">
            <div class="relative flex flex-col sm:flex-row px-5 sm:px-16 lg:px-32 pt-40 pb-10">
                <div class="hidden lg:block sm:w-1/12 items-center mr-5 self-center">
                    <img src="{{ asset('images/logo-um.png') }}" alt="logo_um" srcset="" class="">
                </div>
                <div class="hidden lg:block sm:w-3/12 inline-flex items-center">
                    <p class="font-lobster adil text-white-5 leading-tight">Adil.</p>
                </div>
                <div
                    class="flex flex-col sm:w-6/12 sm:px-5 lg:px-0 text-white-5 font-openSans text-sm sm:text-md font-light sm:font-bold sm:leading-relaxed">
                    <p>Prodi. S1 Pendidikan Teknik Informatika</p>
                    <div class="flex flex-row">
                        <a class="pr-2" href="http://elektro.um.ac.id/">
                            <p>Jurusan Teknik Elektro,</p>
                        </a>
                        <a href="http://ft.um.ac.id/">
                            <p>Fakultas Teknik</p>
                        </a>
                    </div>
                    <a href="https://um.ac.id/">
                        <p>Universitas Negeri Malang</p>
                    </a>
                    <p class="font-light font-openSans text-sm">Jl. Semarang 5 Malang, 65145 Telp. (0341) 551312 Malang,
                        Indonesia</p>
                </div>
                <div
                    class="flex flex-col sm:w-6/12 sm:px-5 lg:px-0 lg:w-3/12 font-openSans text-sm sm:text-md text-white-5 sm:leading-relaxed">
                    <p>Email :akughoni@gmail.com</p>
                    <p>Web : <a href="http://ghoni.id">ghoni.id</a></p>
                    <p>Images Source : </p>
                    <p><a href="https://undraw.co/">UnDraw.co</a></p>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>