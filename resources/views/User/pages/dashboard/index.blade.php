@extends('User.layout.default')
@section('title', 'Adil | Dashboard')

@section('content')
<div class="flex relative justify-center z-20 h-auto ">
    <div class="relative h-auto w-11/12 lg:w-9/12 mt-20">
      <div class="sm:px-5 mt-10 text-center md:text-left">
        <h1 class="text-white-1 font-lato sm:text-4xl">Selamat Datang, <strong>{{ Auth::user()->fullname }}</strong></h1>
        <p class="text-white-2 sm:text-lg">Silahkan melanjutkan pembelajaran sesuai dengan apa yang sudah direkomendasikan
          oleh sistem, semangat!</p>
        <div class="flex flex-no-wrap overflow-x-auto md:flex-row mt-4">
          {{-- card 1 --}}
          <div class="h-32 flex-1 flex-row flex bg-white-1 border-1 border-color2 shadow-lg rounded-lg my-2 mx-1 sm:mx-3 xl:my-0 py-4 sm:py-8">
            <div class="flex flex-row w-3/6 h-full justify-end items-center sm:px-3">
              <span class="flex-1 iconify text-5rem text-color2" data-icon="ion:checkmark-done-circle-outline"
                data-inline="false"></span>
              <h1 class="text-color2 text-6xl">{{ (empty(Auth::user()->pretest)) ? 0 : $pass_count->count() }} </h1>
            </div>
            <div class="flex flex-col w-2/5 h-full border-l-2 border-color2 justify-center">
              <p class="text-xl text-color2 font-lato font-semibold px-5">Courses</p>
              <p class="text-xl text-color2 font-lato font-semibold px-5">Pass</p>
            </div>
          </div>
          <div class="h-32 flex-1 flex-row flex bg-white-1 border-1 border-color2 shadow-lg rounded-lg my-2 mx-1 sm:mx-3 xl:my-0 py-4 sm:py-8 mx-0 xl:mx-2">
            <div class="flex flex-row w-3/6 h-full justify-end items-center px-3">
              <span class="flex-1 iconify text-5rem text-color2" data-icon="fe:bookmark" data-inline="false"></span>
              <h1 class="text-color2 text-6xl">{{ $recomend_count }} </h1>
            </div>
            <div class="flex flex-col w-3/6 h-full border-l-2 border-color2 justify-center">
              <p class="text-xl text-color2 font-lato font-semibold px-5">Recomended</p>
              <p class="text-xl text-color2 font-lato font-semibold px-5">Courses</p>
            </div>
          </div>
          <div class="h-32 flex-1 flex-row flex bg-white-1 border-1 border-color2 shadow-lg rounded-lg my-2 mx-1 sm:mx-3 xl:my-0 py-4 sm:py-8">
            <div class="flex flex-row w-3/5 h-full justify-end items-center px-3">
              <span class="flex-1 iconify text-5rem text-color2" data-icon="ic-round-play-circle-outline"
                data-inline="false"></span>
              <h1 class="text-color2 text-6xl ">{{ $courses_take }}</h1>
            </div>
            <div class="flex flex-col w-2/5 h-full border-l-2 border-color2 justify-center">
              <p class="text-xl text-color2 font-lato font-semibold px-5">Courses</p>
              <p class="text-xl text-color2 font-lato font-semibold px-5">Taken</p>
            </div>
          </div>
        </div>
      </div>
      {{-- jika sudah pretest --}}
      @if(!empty(Auth::user()->pretest))
      <div class="flex flex-col mt-4 bg-white-1 w-full h-auto shadow-md px-5 py-4 sm:py-6 sm:px-10">
        <div class="mb-5">
          <h1 class="font-medium font-lato text-lg text-color2 font-semibold">Course Achievement</h1>
          <div class="flex flex-row mt-2 w-full overflow-x-scroll sm:overflow-x-auto flex-no-wrap  sm:flex-wrap">
            {{-- section --}}
            @forelse ($pass_count as $pass)
            <div class="w-20 sm:w-1/12 my-1"> 
              <div class="flex items-center justify-center h-10 sm:h-10 lg:h-20 rounded-full bg-color2 mb-1">
                <span class="iconify lg:text-4xl text-white-1" data-icon="subway:tick" data-inline="false"></span>
              </div>
              <p class="text-xs text-gray-600 text-center font-bold"> {{ $pass->title }} </p>
            </div>
            <div class="sm:w-8 sm:h-1 bg-color2 mt-2 sm:mt-5 lg:mt-10 rounded mx-2 sm:mx-0"></div>
            @empty
            @endforelse
            @forelse ($recomended as $item)
            <div class="w-20 sm:w-1/12 my-1"> 
              <div class="flex items-center justify-center h-10 sm:h-10 lg:h-20 rounded-full bg-white-1 border-2 border-color2 mb-1">
                <span class="iconify lg:text-4xl text-color2" data-icon="el:play" data-inline="false"></span>
              </div>
              <p class="text-xs text-color2 text-center font-bold"> {{ $item->title }} </p>
            </div>
            <div class="sm:w-8 sm:h-1 bg-color2 mt-2 sm:mt-5 lg:mt-10 rounded mx-2 sm:mx-0"></div>
            @empty
            @endforelse
            <div class="w-20 sm:w-1/12 -ml-2"> 
              <div class="flex items-center justify-center h-10 sm:h-10 lg:h-20 rounded-full">
                <span class="iconify text-5xl lg:text-5rem text-color2" data-icon="ion:checkmark-done-circle" data-inline="false"></span>
              </div>
              <p class="text-xs text-color2 text-center font-bold">All Courses Passed</p>
            </div>
          </div>
        </div>
        <div class="flex flex-col w-full h-auto sm:flex-row">
          <div class="flex-1 h-full sm:mr-10">
            <h1 class="font-medium font-lato text-lg text-color2 font-semibold">Recomended Courses</h1>
            <div class="flex flex-col sm:px-2 mt-2">
              @forelse($recomended as $item)
              <a href="{{ route('user.course.show', ['id' => $item->id ]) }} " class="">
                <div class="w-full border shadow rounded border-gray-300 px-4 py-2 mb-2 bg-white-1 hover:border-color2 hover:shadow-md">
                  <h1 class="font-normal text-color2hover font-lato text-lg">{{ $item->title }} </h1>
                  <div class="font-normal font-openSans text-sm text-gray-600">{!! Str::limit($item->description,100,'...') !!} </div>
                </div>
              </a>
              @empty
                <p class="font-medium text-md text-left text-gray-600 mb-1">You have pass all course, you are the best.</p>
              @endforelse
              @if($recomend_count > 5 )
              <a href="{{ route('user.course.index') }} " class="py-3 rounded text-color2 hover:text-color2hover font-robot text-lg">Selengkapnya...</a>
              @endif
            </div>
          </div>
          <div class="flex-1 h-full mt-4 sm:mt-0">
            <h1 class="font-medium font-lato text-lg text-color2 font-semibold">Last Courses</h1>
            <div class="flex flex-col sm:px-2 mt-2">
              @forelse($last_courses->sortByDesc('historySection.updated_at') as $item)
              <a href="{{ route('user.course.show', ['id' => $item->id ]) }} " class="">
                <div class="w-full border shadow rounded border-gray-300 px-4 py-2 mb-2 bg-white-1 hover:border-color2 hover:shadow-md">
                  <h1 class="font-normal text-color2hover font-lato text-lg">{{ $item->title }} </h1>
                  <p class="font-normal font-openSans text-sm text-gray-600">{{ $item->historySection->updated_at->diffForHumans() }} </p>
                </div>
              </a>
              @empty
                <p class="font-medium text-md text-left text-gray-600 mb-1">You haven't taken a course. let's take it!</p>
              @endforelse
            </div>
          </div>
        </div>
      </div>
      @else
      {{-- jika belum pretest --}}
      <div class="flex flex-col justify-center items-center mt-4 bg-white-1 w-full h-auto md:h-bg20rem shadow-md py-6 px-16 sm:px-24 relative">
        <div class="w-full font-openSans text-center text-gray-800">
          <p class="text-md">Hai, <strong>{{ Auth::user()->fullname }}</strong> kamu belum mengambil Pretest. 
            Silakan kerjakan pretest terlebih dahulu, agar kamu dapat materi yang sesuai dengan kemampuanmu.</p>
        </div>
        <div class="w-auto text-center mt-5">
          <h1 class="text-xl sm:text-xl font-roboto text-gray-900 my-4">Kerjakan Pretest!</h1>
          <a href="{{ route('user.before.pretest') }}" class="px-16 py-2 bg-color2 rounded text-white-1 hover:bg-color2hover">Sekarang</a>
        </div>
      </div>
      @endif
    </div>
  </div>
@endsection

@section('js')
  @include('User.pages.dashboard.js')
@endsection