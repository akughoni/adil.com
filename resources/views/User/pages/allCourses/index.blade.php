@extends('User.layout.default')
@section('title', 'Adil | Courses')

@section('content')
<div class="flex relative justify-center z-20 h-auto ">
  <div class="relative h-auto w-11/12 lg:w-9/12 mt-16">
    <div class="sm:px-5 h-auto mt-8 text-left md:text-center">
      <h1 class="text-white-1 font-lato sm:text-4xl">Ayo! Pelajari Semua materi</h1>
      <p class="text-white-2 font-lato sm:text-lg">
        Silahkan mengambil materi yang sudah direkomendasikan oleh sistem
        berdasarkan hasil pretest, semangat!
      </p>
      <div class="mt-8">
        <a href="{{ route('user.course.index') }}"
          class="text-sm sm:text-md px-10 py-3 bg-tosca rounded text-white-1 hover:bg-teal-700 shadow-md">Rekomendasi Materi</a>
      </div>
    </div>
    <div class="flex flex-wrap mt-8 bg-white-1 w-auto h-auto md:h-auto shadow-md sm:p-2 relative">
      @forelse($sections as $item)
      <div class="sm:w-1/3 px-2">
        <div class="h-auto bg-white-1 hover:shadow-xl shadow-md p-2 my-2">
          <a class="" href="{{ route('user.course.all.show',['id'=> $item->id]) }} ">
            <div class="hover:opacity-88">
              <img class="hidden sm:block sm:object-cover" src="{{ asset($item->image) }} " alt="Image Course" srcset="">
            </div>
            <div class="px-2 mt-2">
              <div class="font-roboto font-semibold text-sm sm:text-lg text-color2">{{ $item->title }}</div>
              <div class="text-sm sm:text-md text-gray-600">{!! Str::limit($item->description,75,'...') !!}</div>
            </div>
          </a>
        </div>
      </div>
      @empty
      <div class="w-full h-bg20rem flex flex-col items-center justify-center ">
        <h1 class="text-lg sm:text-2xl font-bold text-gray-500">Courses is Empty</h1>
      </div>
      @endforelse
    </div>
  </div>
</div>
@endsection

@section('js')
@include('User.pages.courses.js')
@endsection