@extends('User.layout.default')
@section('title', 'Adil | Courses')
@section('h-bg','h-rem12')
@section('content')
<div class="flex relative justify-center z-20 min-h-screen">
  <div class="relative h-auto w-12/12 lg:w-9/12 mt-16">
    <div class="px-2 sm:px-5 h-auto mt-5 sm:mt-12 text-center">
      <h1 class="text-white-1 font-lato text-lg sm:text-4xl uppercase">{{ $section->title }} </h1>
    </div>
    <div class="flex flex-col-reverse sm:flex-row-reverse mt-8 bg-white-1 w-auto h-auto shadow-md sm:p-2 relative">
      <div class="flex flex-col sm:w-6/12 bg-white-1 h-auto">
        @foreach($lectures->sortBy('ordering') as $key => $item)
        <a href="{{ route('user.course.video',['section' => $section->id, 'page' => $key+1]) }} " class="">
          <div class="flex flex-row w-full border rounded shadow border-gray-400 h-auto hover:bg-gray-100 hover:shadow-md">
            <div class="w-11/12 flex flex-col items-stretch px-4 py-2">
              <h1 class="text-sm sm:text-md font-lato text-gray-800">{{ $item->title }} </h1>
            </div>
            <div class="w-auto flex flex-col items-end px-5 py-2">
              @if($item->recomendedLecture->taken)
              <span class="iconify text-3xl sm:text-4xl text-tosca" data-icon="ion:checkmark-done-circle-outline" data-inline="false"></span>
              @else
              <p class="text-sm sm:text-md text-gray-600 font-openSans">Durasi</p>
              <p class="text-sm sm:text-md text-gray-600 font-openSans">{{ $item->duration }} </p>
              @endif
            </div>
          </div>
        </a>
        @endforeach
      </div>
      <div class="sm:w-6/12 flex flex-col justify-start px-4 py-2 bg-white-1 ml-2">
        <p class="text-md sm:text-xl font-roboto font-semibold text-gray-800">{{ $section->title }}</p>
        <p class="text-sm sm:text-md font-openSans text-gray-600">{{ $section->created_at->diffForHumans() }}</p>
        <div class="text-sm sm:text-md p-2 font-roboto text-gray-800">
          {!! $section->description !!}
        </div>
        <a href="{{ URL::previous() }}" class="mt-2 px-6 py-2 bg-color2 text-sm sm:text-lg font-lato text-white-2 rounded text-center hover:bg-color2hover hover:text-white-1">Back</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
@include('User.pages.courses.js')
@endsection