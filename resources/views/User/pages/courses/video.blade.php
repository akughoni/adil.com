@extends('User.layout.default')
@section('title', 'Adil | Courses')
@section('h-bg','h-rem36')
@section('content')
<div class="flex flex-col relative items-center z-20 min-h-screen">
  <div class="flex flex-col lg:flex-row w-full relative h-auto mt-16 px-4 lg:px-10">
    @foreach ($data as $lecture)
    <div class="sm:px-5 h-auto mt-8 text-center lg:w-8/12">
      <iframe controls class="w-full h-64 sm:h-rem28" src="{{ $lecture->video_url }}" allowfullscreen>
      </iframe>
      <h1 class="text-white-1 font-lato text-lg sm:text-3xl">{{ $lecture->title }} </h1>
      <div class="mt-2 flex flex-row justify-center">
        {{ $data->links('vendor.pagination.course.video') }}
        @if($list_lecture->last()->id == $lecture->id)
          <a href="{{ route("user.session.start",['section' => $section_id]) }}" class="mx-2 px-6 py-1 bg-tosca text-sm sm:text-lg font-lato text-white-2 rounded text-center hover:bg-teal-800 hover:text-white-1">Try Session Test!</a>
        @endif
      </div>
    </div>
    <div class="flex flex-col mt-10 sm:mt-8 lg:w-4/12">
      <div class="h-auto sm:h-rem28 overflow-y-scroll bg-white-2">
        @foreach($list_lecture as $key => $item)
        <a href="{{ route('user.course.video', ['section' => $section_id, 'page' => $key+1 ]) }} " class="">
          <div class="flex flex-row w-full border shadow {{ $item->id == $lecture->id ? "border-color2" : "border-gray-400" }} h-auto my-0">
            <div class="w-10/12 flex flex-col items-stretch px-4 py-2">
              <h1 class="text-sm sm:text-md font-lato {{ $item->id == $lecture->id ? "text-color2" : "text-gray-800" }} ">{{ $item->title }} </h1>
            </div>
            <div class="w-auto flex flex-col items-stretch px-4 py-2 text-right">
              @if($item->recomendedLecture->taken)
              <span class="iconify text-4xl text-color2" data-icon="ion:checkmark-done-circle-outline" data-inline="false"></span>
              @else
              <p class="text-sm {{ $item->id == $lecture->id ? "text-color2" : "text-gray-800" }} font-openSans">Durasi</p>
              <p class="text-sm {{ $item->id == $lecture->id ? "text-color2" : "text-gray-800" }} font-openSans">{{ $item->duration }}</p>
              @endif
            </div>
          </div>
        </a>
        @endforeach
      </div>
      <div class="w-full">
        <a href="{{ route('user.course.index') }}">
          <p class="py-3 bg-tosca text-sm sm:text-lg font-lato text-white-2 text-center hover:bg-teal-700 hover:text-white-1">Back to Courses</p> 
        </a>
      </div>
    </div>
  </div>
  <div class="relative h-auto w-11/12 lg:w-8/12">
    <div class="flex flex-col sm:flex-row mt-8 bg-white-1 w-auto h-auto shadow-md sm:p-2 relative">
      <div class="w-full sm:w-9/12 flex flex-col justify-start px-2 sm:px-4 py-1 sm:py-2 bg-white-1 sm:ml-2">
        <p class="sm:text-lg font-roboto font-semibold text-gray-800">{{ $lecture->title }}</p>
        <p class="text-sm sm:text-md font-openSans text-gray-600">{{ $lecture->created_at->diffForHumans() }}</p>
        <div class="text-sm sm:text-md p-1 sm:p-2 font-roboto text-gray-800">
          {!! $lecture->description !!}
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection

@section('js')
@include('User.pages.courses.js')
@endsection