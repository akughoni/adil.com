@extends('User.layout.default')
@section('title', 'Adil | Courses')

@section('content')
<div class="flex relative justify-center z-20 h-auto ">
  <div class="relative h-auto w-11/12 lg:w-9/12 mt-16">
    <div class="sm:px-5 h-auto mt-8 text-left md:text-center">
      <h1 class="text-white-1 font-lato text-md sm:text-4xl">Materi yang Direkomendasikan.</h1>
      <p class="text-white-2 font-lato text-sm sm:text-lg">
        Silahkan mengambil materi yang sudah direkomendasikan oleh sistem
        berdasarkan hasil pretest, semangat!
      </p>
      <div class="mt-4 sm:mt-8">
        <a href="{{ route('user.course.all') }}"
          class="px-8 py-2 sm:px-16 sm:py-3 bg-tosca rounded text-white-1 hover:bg-teal-700 shadow-md">Semua Materi</a>
      </div>
    </div>
    <div class="flex flex-wrap mt-8 bg-white-1 w-auto h-auto md:h-auto shadow-md sm:p-2 relative">
      @forelse($sections as $item)
      <div class=" sm:w-1/3 px-2">
        <div class="h-auto bg-white-1 hover:shadow-xl shadow-md p-2 my-2">
          <a class="" href="{{ route('user.course.show',['id'=> $item->id]) }} ">
            <div class="hover:opacity-88">
              <img class="hidden sm:block sm:object-cover" src="{{ asset($item->image) }} " alt="Image Course" srcset="">
            </div>
            <div class="px-2 mt-2">
              <div class="font-roboto font-semibold text-md sm:text-lg text-color2">{{ $item->title }}</div>
              <div class="text-sm sm:text-md text-gray-600">{!! Str::limit($item->description,75,'...') !!}</div>
            </div>
          </a>
        </div>
      </div>
      @empty
      <div class="w-full h-bg20rem flex flex-col items-center justify-center ">
        <h1 class="text-2xl font-bold text-gray-500">Tidak Ada Rekomendasi Materi untuk Kamu!</h1>
        @if(empty(Auth::user()->pretest))
          <h1 class="text-lg text-gray-500">Kamu belum mengambil pretest!</h1>
          <a href="{{ route('user.before.pretest') }}" class="my-2 px-12 py-2 bg-color2 rounded text-white-1 hover:bg-color2hover">Ayo Kerjakan!</a>
        @else 
          <h1 class="text-lg text-gray-500">Kamu sudah lulus semua materi yang direkomendasikan!</h1>
        @endif
      </div>
      @endforelse
      {{ $sections->links('vendor.pagination.course.index') }}

    </div>
  </div>
</div>
@endsection

@section('js')
@include('User.pages.courses.js')
@endsection