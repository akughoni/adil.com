<script src="{{ asset('js/all.js') }}"></script>

<script>
    let urlGetData = "{{ route('user.session.start',['section'=> $section_id ?? '']) }}";
    let urlSubmit = "{{ route('user.session.submit') }}";
    let data = [];

    function getData(url = urlGetData) {
        $.ajax({
            method: 'GET',
            url: url,
            success: function (res) {                  
                $('#question').html(res);
            }
        }).done(() => {
            checked_answer();
            number_answer();
        });
    }

    function number_answer(){
        data.forEach(item => {
            $('.pagination .number #'+item.number).addClass('bg-color2 text-white-1');
            $('.pagination .number #'+item.number).removeClass('bg-gray-400');
        });
    }

    $(document).on('click', '.pagination .number', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        getData(url);
    });

    $(document).on('click', 'nav .pagination .link', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        getData(url);
    });

    $(document).ready(function () {
        // push user answer on array obj
       $(document).on('change','input[type="radio"]',function (e) { 
            e.preventDefault();
            const radio = $('input[type="radio"]');
            const checked = radio.filter(function(item,index, arr){ //find html radio button on checked
                return $(this).prop('checked');
            })
            const questionId = checked.attr('name');
            const number = $('#'+questionId).attr('number');
            const value = checked.val();
            const answer = {
                id : questionId,
                value : value,
                number : number
            }

            if(data.length == 0){
                data.push(answer);
            }else{
                const found = data.some(item => item.id === questionId); //return true or false from condition
                if(found){
                    // make new array obj with update value where data.id == questionId
                    data = data.map((item) => {
                        if(item.id === questionId){
                            item.value = value;
                            return item
                        }
                        return item;
                    });
                }else{
                    data.push(answer);
                }
            }
            // console.log(data);
       });
    });

    // keep selected user anwer after ajax request
    function checked_answer(){
        const radio = $('input[type="radio"]');
        const currentId = $(radio).attr('name');
        let input ={};
        let value =null;
        let element = data.filter((item) => item.id === currentId );
        element.forEach(el => {
            value = el.value;
        })
        selector = 'input[value="'+value+'"]';
        $(selector).prop('checked', true);
    }

    //submit jawaban
    $(document).on('click','#submit', (e) => {
        if(data.length == '{{ $count ?? '' }}'){
            alertify.confirm('Apakah anda yakin submit jawaban anda?', 
            function(e){ 
                $.ajax({
                    type: "POST",
                    url: urlSubmit,
                    data: {
                        _token: "{{ csrf_token() }}",
                        data : data,
                        section_id: "{{ $section_id ?? '' }}"
                    },
                    dataType: 'HTML',
                    success: function(res){
                        $("#question").html(res);
                        $('#title_page').html("Hasil Session Test!");
                        $('#desc_page').html("Berikut adalah report hasil session test yang sudah kamu lakukan, kamu dapat melihat mana topik yang belum kamu kuasai!!");
                    }
                }).done((res)=> {
                    // window.location.href = "{{ route('user.session.report') }}" + res.result ;
                    alertify.success('Session Test Berhasil');
                }).fail(()=> {
                    alertify.cancel('Session Test Gagal');
                });
            }).setHeader('<p></p>'); ;
        }else{
            alertify.alert().set('message', '<p class="text-gray-700 text-md font-roboto">Anda Belum Menjawab Keseluruhan Soal! Silakan Periksa Kembali Jawaban Anda.')
                            .show()
                            .setHeader('<p></p>'); 
        }

    });
</script>