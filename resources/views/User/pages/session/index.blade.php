@extends('User.layout.default')
@section('title', 'Session Test')

@section('content')
  <div id="app" class="flex relative justify-center z-20 h-auto ">
    <div class="relative h-auto w-full mx-2 sm:mx-0 sm:w-10/12 mt-20">
      <div class="px-2 sm:px-20 mt-10 text-center">
        <h1 id="title_page" class="text-white-1 font-lato text-2xl sm:text-4xl">Kerjakanlah Soal-soal Berikut!</h1>
        <p id="desc_page" class="text-white-2 hidden sm:block sm:text-md">Hasil dari Session Test ini akan menjadi penentu rekomendasi materi berikutnya!</p>
      </div>
      <div id="question" class="flex flex-col-reverse lg:flex-row mt-4 bg-white-1 w-full h-auto shadow-md py-6 px-4 sm:px-10 justify-start ">
        @include('User.pages.session.question')
      </div>
    </div>
  </div>
@endsection

@section('js')
  @include('User.pages.session.js')
@endsection