@extends('User.layout.default')
@section('title', 'Adil | User Profile')
@section('h-bg','h-40')
@section('content')
<div class="flex relative justify-center z-20 h-auto ">
  <div class="relative h-auto w-11/12 lg:w-9/12 mt-20">
    <div class="sm:px-5 mt-5 sm:mt-10 text-center md:text-left">
      <h1 class="text-white-1 text-center text-lg sm:text-3xl">My Profile</h1>
    </div>
    <div class="flex flex-col sm:flex-row mt-4 bg-white-1 w-full h-auto shadow-md relative p-5">
      <div class="sm:block w-full sm:w-3/12 sm:pl-8 border-b sm:border-b-0 sm:border-r border-gray-400 font-roboto text-gray-800">
        <a id="basic" class="cursor-pointer block my-1 sm:my-4 text-color2 hover:bg-gray-100 p-2 rounded hover:border-l-2 hover:border-color2">Basic Info</a>
        <a id="change" class="cursor-pointer block my-1 sm:my-4 text-color2 hover:bg-gray-100 p-2 rounded hover:border-l-2 hover:border-color2">Change Password</a>
      </div>
      <div class="w-full sm:w-9/12 sm:px-8">
        @if(session('status'))
          <div class="w-full px-3 py-2 bg-{{ session('color') }}-200 text-{{ session('color') }}-900 text-sm rounded opacity-88">
            {{ session('status') }}
          </div>
        @endif
        @if(count($errors) > 0)
          <div class="w-full px-3 py-2 bg-red-200 text-red-900 text-sm rounded opacity-88">
            <ul>
              @foreach ($errors->all() as $error)
                  <ol>{{ $error }}</ol>
              @endforeach
            </ul>
          </div>
        @endif
         <div id="content"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  @include('User.pages.profile.js')
@endsection