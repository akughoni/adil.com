<script src="{{ asset('js/all.js') }} "></script>
<script type="text/javascript">
  $(document).ready(function () {
    getContent();

    $('#password').submit(function (e) { 
      e.preventDefault();
      alert('Hai');
    });
  });

  $('#basic').click(function (e) { 
    e.preventDefault();
    getContent();
  });
  $('#change').click(function (e) { 
    e.preventDefault();
    getContent("{{ route('profile.change') }} ");
  });

  function getContent(url = "{{ route('profile.basic') }}"){
    $.ajax({
      type: "GET",
      url: url,
      dataType: "html",
      success: function (res) {
        $('#content').html(res);
      }
    });
  }



</script>