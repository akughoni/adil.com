{{-- <form id="password" action="" method="post"> --}}
<form action="{{ route('profile.update.password') }}" method="post">
  @csrf
  <div class="flex flex-col items-start w-full my-4">
    <label for="old" class="text-color2 py-2">Old Password</label>
    <input class="bg- px-4 py-2 rounded w-full border border-gray-400 focus:border-color2 text-gray-800" type="password" placeholder="Your Password"  name="old" id="old">
  </div>
  <div class="flex flex-col items-start w-full my-4">
    <label for="new" class="text-color2 py-2">New Password</label>
    <input class="bg- px-4 py-2 rounded w-full border border-gray-400 focus:border-color2 text-gray-800" type="password" placeholder="New Password" name="new" id="new">
  </div>
  <div class="flex flex-col items-start w-full my-4">
    <label for="confirm" class="text-color2 py-2">Confirm New Password</label>
    <input class="bg- px-4 py-2 rounded w-full border border-gray-400 focus:border-color2 text-gray-800" type="password" placeholder="Confirm New Password" name="confirm" id="confirm">
  </div>
  <div class="flex flex-col items-start w-full my-6">
    <button type="submit" class="flex flex-row bg-color2 px-5 py-2 rounded text-white-1 hover:bg-color2hover">
      <span class="iconify text-xl mx-1" data-icon="ic:round-edit" data-inline="false"></span>
      Change Password
    </button>
  </div>
</form>