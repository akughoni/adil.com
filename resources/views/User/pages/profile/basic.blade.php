<form action="{{ route('profile.store') }}" method="post">
  @csrf
  <div class="flex flex-col items-start w-full my-4">
    <label for="fullname" class="text-color2 py-2">Fullname</label>
    <input class="text-sm sm:text-md px-2 sm:px-4 py-2 rounded w-full border border-gray-400 focus:border-color2 text-gray-800" type="text" value="{{ Auth::user()->fullname }}" name="fullname" id="fullname">
  </div>
  <div class="flex flex-col items-start w-full my-4">
    <label for="fullname" class="text-color2 py-2">NIM</label>
    <input class="text-sm sm:text-md px-2 sm:px-4 py-2 rounded w-full border border-gray-400 focus:border-color2 text-gray-800" type="text" value="{{ Auth::user()->nim }}" name="nim" id="nim">
  </div>
  <div class="flex flex-col items-start w-full my-4">
    <label for="fullname" class="text-color2 py-2">Email</label>
    <input class="text-sm sm:text-md px-2 sm:px-4 py-2 rounded w-full border border-gray-400 focus:border-color2 text-gray-800" type="text" value="{{ Auth::user()->email }}" name="email" id="email">
  </div>
  <div class="flex flex-col items-start w-full my-6">
    <button type="submit" class="flex flex-row text-sm sm:text-md bg-color2 px-5 py-2 rounded text-white-1 hover:bg-color2hover">
      <span class="iconify text-xl mx-1" data-icon="ic:round-edit" data-inline="false"></span>
      Save
    </button>
  </div>
</form>