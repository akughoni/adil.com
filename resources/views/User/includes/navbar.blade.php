<nav
  class="flex md:mx-0 z-50 fixed top-0 md:flex-row px-10  md:px-40 h-16 sm:h-20 w-full items-center bg-tosca shadow-nav">
  <div class="w-1/4 lg:w-2/12 inline-flex items-center">
    <img src="{{ asset('images/logo-um.png') }}" alt="logo_um" srcset="" class="w-12 h-12">
    <div class="w-1 h-12 bg-white-2 mx-2"></div>
    <a href="{{ route('user.dashboard') }}">
      <h1 class="font-lobster text-white-2 text-3xl sm:text-5xl text-center md:text-left">Adil.</h1>
    </a>
  </div>
  <div class="w-8/12 h-auto justify-center items-center hidden lg:flex">
    <a href="{{ route('user.dashboard') }}" class="px-3 mx-1 text-white-1 font-lato py-1 rounded">
      <span class="iconify inline text-xl" data-icon="ant-design:dashboard-filled" data-inline="false"></span>
      <span class="inline text-md">Dashboard</span>
    </a>
    <a href="{{ route('user.course.index') }} " class="px-3 mx-1 text-white-1 font-lato py-1 rounded">
      <span class="iconify inline text-xl" data-icon="ic:baseline-ondemand-video" data-inline="false"></span>
      <span class="inline">Recomended</span>
    </a>
    <a href="{{ route('user.course.all') }} " class="px-3 mx-1 text-white-1 font-lato py-1 rounded">
      <span class="iconify inline text-xl" data-icon="ic:outline-video-library" data-inline="false"></span>
      <span class="inline">All Courses</span>
    </a>
  </div>
  <div class="w-3/4 lg:w-3/12 h-full flex justify-end items-center relative">
    <div class="w-full flex flex-col text-right">
      <a href="{{ route('profile.index') }}"
        class="px-2 text-white-1 font-lato hidden sm:block leading-tight">{{ Auth::user()->fullname }}</a>
      <a href="{{ route('profile.index') }}"
        class="px-2 text-white-5 font-lato hidden sm:block">{{ Auth::user()->nim }}</a>
    </div>
    <div id="userMenu" class="absolute shadow px-2 py-2 bg-white-1 mt-32 md:mt-16 mx-2 rounded flex flex-col w-32">
      <a href="{{ route('user.dashboard') }}"
        class="py-2 px-1 text-gray-800 hover:bg-gray-100 rounded lg:hidden">Dashboard</a>
      <a href="{{ route('user.course.index') }}"
        class="py-2 px-1 text-gray-800 hover:bg-gray-100 rounded lg:hidden">Recomended</a>
      <a href="{{ route('user.course.all') }}" class="py-2 px-1 text-gray-800 hover:bg-gray-100 rounded lg:hidden">All
        Courses</a>
      <a href="{{ route('profile.index') }}" class="py-2 px-1 text-gray-800 hover:bg-gray-100 rounded">Profile</a>
      <a href="{{ route('auth.logout') }}" class="py-2 px-1 text-gray-800 hover:bg-gray-100 rounded">Logout</a>
    </div>
    <div class="h-full flex flex-col justify-center md:relative">
      <div class="w-full">
        <a id="userIcon" href="">
          <span class="iconify text-3xl text-white-1" data-icon="fa-regular:user-circle" data-inline="false"></span>
        </a>
      </div>

    </div>
  </div>
</nav>