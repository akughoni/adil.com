<footer>
  <div class="block flex w-full h-16 z-10  bg-color2 mt-20 justify-center items-center">
      <h1 class="text-sm sm:text-md text-white-1 font-openSans">Develops By Ghoni-Jee</h1>
  </div>
</footer>
<script src="{{ asset('js/all.js') }}"></script>

<script>
$(document).ready(function () {
  $('#userMenu').hide();
});

$("#userIcon").click(function (e) { 
  e.preventDefault();
  $('#userMenu').toggle();
});

</script>