@if ($paginator->hasPages())
    <nav class="w-full">
        <div class="pagination flex flex-row justify-center">
            <div class="px-2 bg-white-1 text-blue-700 rounded-l-lg">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                    <p class="disabled text-blue-300" aria-disabled="true"><span>@lang('pagination.previous')</span></p>
                @else
                    <a class="text-blue-700" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
                @endif
            </div>
            <div class="px-2 bg-white-1 text-blue-700 rounded-r-lg">
                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <a class="text-blue-700" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
                @else
                    <p class="disabled text-blue-300" aria-disabled="true"><span>@lang('pagination.next')</span></p>
                @endif
            </div>
        </div>
    </nav>
@endif
