@if ($paginator->hasPages())
{{-- Pagination Elements --}}
    @foreach ($elements as $element)
    {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                {{-- <a href=""> --}}
                <div  class="w-10 h-10 bg-gray-100 border border-color2 rounded-full m-2 text-center flex justify-center items-center">
                   {{ $page }}
                </div>
                {{-- </a> --}}
                @else
                <a class="number" href="{{ $url}}">
                    <div id="{{ $page }}" class="w-10 h-10 bg-gray-400 rounded-full m-2 text-center flex justify-center items-center">
                        {{ $page }}
                    </div>
                </a>
                @endif
            @endforeach
        @endif
    @endforeach
@endif
