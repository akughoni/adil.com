@if ($paginator->hasPages())
<div class="w-auto">
    <div class="pagination flex flex-row justify-center"> 
        @if ($paginator->onFirstPage())
            <p class="disabled px-6 sm:px-12 py-1 bg-gray-300 rounded text-tosca text-lg mx-2" aria-disabled="true">Back</p>
        @else
        <a rel="prev" href="{{ $paginator->previousPageUrl() }}"
            class="px-6 sm:px-12 py-1 bg-tosca rounded text-white-1 text-lg mx-2">Back</a>
        @endif
            {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="px-6 sm:px-12 py-1 bg-tosca rounded text-white-1 text-lg mx-2">Next</a>
        @else
            <p class="disabled px-6 sm:px-12 py-1 bg-gray-300 rounded text-tosca text-lg mx-2" aria-disabled="true">Next</p>
        @endif
    </div>
</div>
@endif