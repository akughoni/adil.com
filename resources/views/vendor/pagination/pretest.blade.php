@if ($paginator->hasPages())
    <nav class="w-full lg:absolute">
        <div class="pagination flex flex-row justify-start ">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <div class="mx-2 my-1 sm:my-0 px-6 py-2 bg-white-5 flex items-center text-tosca rounded">
                    <span class="iconify text-tosca text-xl" data-icon="ic:round-arrow-back" data-inline="false"></span><p class="hidden sm:block">Kembali</p>
                </div>
                @else
                <div class="mx-2 my-1 sm:my-0 px-6 py-2 bg-tosca hover:text-white-1 hover:bg-teal-700 flex flex-row items-center text-white-2 rounded">
                    <a class="link flex flex-row" href="{{ $paginator->previousPageUrl() }}" rel="prev">
                        <span class="iconify text-white-2 text-xl" data-icon="ic:round-arrow-back" data-inline="false"></span><p class="hidden sm:block">Kembali</p>
                    </a>
                </div>
                @endif
            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <div class="mx-2 my-1 sm:my-0 px-6 py-2 bg-tosca hover:text-white-1 hover:bg-teal-700 flex items-center text-white-2 rounded">
                    <a class="link flex flex-row" href="{{ $paginator->nextPageUrl() }}" rel="next">
                        <p class="hidden sm:block">Selanjutnya</p><span class="iconify text-white-2 text-xl" data-icon="ic:round-arrow-forward" data-inline="false"></span> 
                    </a>
                </div>
            @else
                <div class="mx-2 my-1 sm:my-0 px-6 py-2 bg-white-5 flex items-center text-tosca rounded">
                    <p class="hidden sm:block">Selanjutnya</p> <span class="iconify text-tosca text-xl" data-icon="ic:round-arrow-forward" data-inline="false"></span> 
                </div>
            @endif
        </div>
    </nav>
@endif
