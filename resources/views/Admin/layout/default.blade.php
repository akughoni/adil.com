<!DOCTYPE html>
<html>
<head>
  @include('Admin.includes.head')
  <title>@yield('title')</title>
</head>
<body>
  <!--Container -->
  <div class="mx-auto bg-grey-400">
      <!--Screen-->
      <div class="min-h-screen flex flex-col">
          <!--Header Section Starts Here-->
        @include('Admin.includes.navbar')
          <!--/Header-->
  
          <div class="flex flex-1">
              <!--Sidebar-->
            @include('Admin.includes.sidebar')
              <!--/Sidebar-->
              <!--Main-->
            @yield('content')
              <!--/Main-->
          </div>
          <!--Footer-->
          @include('Admin.includes.footer')
          <!--/footer-->
  
      </div>
  
  </div>
  @yield('js')
  <script>
    $('#content').click(function (e) { 
      e.preventDefault();
        // $('#content-icon').toggleClass("hidden");
      $('#content-icon').switchClass('fa-angle-right', 'fa-angle-down', 100);
      $('#content-icon.fa-angle-down').switchClass('fa-angle-down', 'fa-angle-right', 100);
      $('#sidebar-courses').slideToggle(300);
    });
  </script>
  </body>
</html>