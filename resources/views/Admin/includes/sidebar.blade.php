<aside id="sidebar" class="bg-side-nav w-1/2 md:w-1/6 lg:w-1/6 border-r border-side-nav hidden md:block lg:block">
    <ul class="list-reset flex flex-col">
        {{-- <li class=" w-full h-full py-3 hover:bg-white px-2 border-b border-light-border @yield('sidebar-dashboard') ">
            <a href="{{ route('AdminDashboard') }}" class="font-sans font-roboto hover:font-normal text-sm text-nav-item no-underline">
                <i class="fas fa-tachometer-alt float-left mx-2"></i>
                Dashboard
                <span><i class="fas fa-angle-right float-right"></i></span>
            </a>
        </li> --}}
        <li class="w-full h-full py-3 hover:bg-white px-2 border-b border-light-border @yield('sidebar-user')">
            <a href="{{ route('AdminUser') }}" class="font-sans font-roboto hover:font-normal text-sm text-nav-item no-underline">
                <i class="fas fa-users float-left mx-2"></i>
                Users
                <span><i class="fa fa-angle-right float-right"></i></span>
            </a>
        </li>
        <li id="content" class="w-full h-full hover:bg-white py-3 px-2 border-b border-light-border 
            @yield('sidebar-courses') @yield('sidebar-sections') @yield('sidebar-lectures')">
            <a href="#" class="font-sans font-roboto hover:font-normal text-sm text-nav-item no-underline">
                <i class="fas fa-grip-horizontal float-left mx-2"></i>
                    Content Management
                <span><i id="content-icon" class="fa fa-angle-right float-right"></i></span>
                {{-- <span><i id="content-icon" class="fa fa-angle-down float-right hidden"></i></span> --}}
            </a>
        </li>
        <div id="sidebar-courses" class="hidden">
            <li id="" class="w-full h-full py-3 px-12 border-b border-light-border hover:bg-white-1 @yield('sidebar-courses')">
                <a href="{{ route('AdminCourses') }}" class="font-sans font-roboto hover:font-normal text-sm text-gray-600 text-nav-item no-underline">
                    Courses
                </a>
            </li>
            <li id="" class="w-full h-full py-3 px-12 border-b border-light-border hover:bg-white-1 @yield('sidebar-sections')">
                <a href="{{ route('AdminSections') }}" class=" font-sans font-roboto hover:font-normal text-sm text-gray-600 text-nav-item no-underline">
                    Sections
                </a>
            </li>
            <li id="" class="w-full h-full py-3 px-12 border-b border-light-border hover:bg-white-1 @yield('sidebar-lectures')">
                <a href="{{ route('AdminLecture') }}" class=" font-sans font-roboto hover:font-normal text-sm text-gray-600 text-nav-item no-underline">
                    Lectures
                </a>
            </li>
        </div>
        {{-- <li id="sidebar-" class="w-full h-full py-3 px-2 border-b border-light-border hover:bg-white @yield('sidebar-pretest')">
            <a href="{{ route('pre-tests.index') }}" class="font-sans font-roboto hover:font-normal text-sm text-nav-item no-underline">
                <i class="fas fa-table float-left mx-2"></i>
                Pre-Test
                <span><i class="fa fa-angle-right float-right"></i></span>
            </a>
        </li> --}}
        <li id="sidebar-" class="w-full h-full py-3 px-2 border-b border-light-border hover:bg-white @yield('sidebar-sessions')">
            <a href="{{ route('session-tests.index') }}" class="font-sans font-roboto hover:font-normal text-sm text-nav-item no-underline">
                <i class="fab fa-uikit float-left mx-2"></i>
                Questions Test
                <span><i class="fa fa-angle-right float-right"></i></span>
            </a>
        </li>
        <li id="sidebar-" class="w-full h-full py-3 px-2 border-b border-light-border hover:bg-white @yield('sidebar-logout')">
            <a href="{{ route('auth.logout') }}" class="font-sans font-roboto hover:font-normal text-sm text-nav-item no-underline">
                <i class="fas fa-sign-out-alt float-left mx-2"></i>
                Logout
            </a>
        </li>
    </ul>

</aside>