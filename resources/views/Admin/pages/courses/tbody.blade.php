<div class="px-3 py-3 flex justify-center">
  <table class="w-full text-sm bg-white shadow-md rounded mb-4 text-gray-800">
    <thead>
      <tr class="border-b">
        <th class="text-center py-3 px-3 w-auto">
          <span class="text-xs">All</span><br>
          <input type="checkbox" onClick="select_all(this)" />
        </th>
        <th class="text-left p-3 px-5 w-1/12">No.</th>
        <th class="text-left p-3 px-5 w-1/12">ID</th>
        <th class="text-left p-3 px-5 w-5/12">Title</th>
        <th class="text-center p-3 px-5 w-3/12">Amount Of Section</th>
        <th class="text-center p-3 px-5 w-1/12">Status</th>
        <th class="text-center p-3 px-5 w-1/12">Actions</th>
      </tr>
    </thead>
    <tbody>
    @if($data->count() > 0)
      @foreach ($data as $key => $course)
        <tr class="border-b hover:bg-gray-100 @if(!$course->status) bg-gray-100 text-gray-500 @else text-gray-700  @endif" id="content">
          <td class="py-3 px-3 text-center">
            <input type="checkbox" name="select_delete" id="multiple" value="{{ $course->id }}"> 
          </td>
          <td class="p-3 px-5">{{ $course->ordering }}</td>
          <td class="p-3 px-5">{{ $course->id }}</td>
          <td class="p-3 px-5">{{ $course->title }}</td>
          <td class="p-3 text-center px-5">{{ $course->sections->count() }}</td>
          <td class="p-3 text-center px-5">
            <a href="{{ route("AdminCourseActive", ["id"=> $course->id ]) }}">
              <div class="relative w-10">                
                <div id="line_toggle" class="w-full h-4 rounded-lg bg-gray-500 shadow-inner my-1"></div>
                <label>
                  <div id="{{ $course->id }}" class="w-6 h-6 rounded-full shadow-inner absolute top-0 -my-1 @if($course->status) bg-teal-600 right-0 @else bg-gray-200 left-0 @endif"></div>
                </label>
              </div>
            </a>
          </td>
          <td class="p-3 text-center px-5 flex">
            <a id="btn-edit" href="{{ route("AdminCourseEdit", ["id"=> $course->id ]) }}"
              class="mr-3 text-sm bg-orange-500 hover:bg-orange-700 text-white py-1 px-2 rounded">Edit</a>
            <button onclick="btn_del('{{ $course->id }}')"
              class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded">Delete</button>
          </td>
        </tr>
      @endforeach
    @else
      <tr class="border-b hover:bg-gray-100 text-gray-700">
        <td colspan="6" class="p-3 px-5 text-center">Courses Not Found</td>
      </tr>
    @endif
    </tbody>
  </table>
</div>
<div class="w-full h-16 bg-white flex items-center">
  {{ $data->links('vendor.pagination.pagination') }}
</div>