@extends('Admin.layout.default')
@section('sidebar-courses', 'bg-white')

@section('title','Admin Dashboard')

@section('content')
<main class="bg-white-100 flex-1 p-3 overflow-hidden">
  <div class="text-gray-900 bg-gray-100">
    <div class="p-4 flex w-full bg-white">
      <h1 class="text-3xl mr-auto text-gray-700">
        Courses
      </h1>
      @error('file')
      <div class="h-auto flex items-center mx-2 bg-red-200 rounded px-3">
        <span class="text-red-900 text-sm align-middle">{{ $message }}</span>
      </div>
      @enderror
      <button class="" onclick="multiple_del()">
        <span class="bg-red-600 hover:bg-red-700 text-white text-sm py-3 px-4 rounded mr-2">Delete Selected</span>
      </button>
      <button data-modal='centeredFormModal'
        class="modal-trigger bg-blue-600 hover:bg-blue-700 text-white text-sm py-2 px-4 rounded">
        Import Data
      </button>
      <span class="px-3 py-2 bg-green-600 text-white rounded ml-2 hover:bg-green-700">
        <a href="{{route('AdminCourseAdd')}}" class="text-sm ">Add New</a>
      </span>

      <div class="mr-0">
        <label for="search" class="px-5 text-gray-700">Serach</label>
        <input type="text" name="search" id="search" class="px-2 py-2 border-gray-400 border rounded">
      </div>
    </div>
    <hr>
    <div class="table">
      @include('Admin.pages.courses.tbody')
    </div>
  </div>
  </div>
</main>
@include('Admin.pages.courses.modal')
@endsection

@section('js')
@include('Admin.pages.courses.js')
@endsection