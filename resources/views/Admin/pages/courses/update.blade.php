@extends('Admin.layout.default')
@section('sidebar-courses', 'bg-white')

@section('title','Admin Create Course')

@section('content')
<main class="bg-white-300 flex-1 mx-5 my-5 overflow-x-hidden">
    <div class="text-gray-900 bg-gray-100">
        <div class="mx-4 py-2 flex w-full">
            <h1 class="text-2xl mr-auto border-b border-gray-400 text-gray-700 pb-2 w-full">
                Add New Course
            </h1>
        </div>
        @if (count($errors) > 0)
        <div class="mx-4 p-3 bg-red-300 rounded">
            <ul>
                @foreach ($errors->all() as $error)
                    <ol class="text-sm">{{ $error }}</ol>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="px-5 flex w-full">
            <form action="{{ route('AdminCourseUpdate') }}" method="post" class="w-full" enctype="multipart/form-data"
                id="created">
                @method('PATCH')
                @csrf
                <input type="text" class="hidden" name="id" value="{{ old('id') ?? $course->id }}">
                <div class="flex flex-row">
                    <div class="flex flex-col w-full my-2">
                        <label for="title" class="text-gray-700 font-roboto pb-3">Title</label>
                        <input type="text" name="title" id="title" value="{{ old('title') ?? $course->title}}"
                            class="w-full border border-gray-400 px-2 py-2 rounded font-lato" placeholder="Title Course">
                        <p id="erorTitle" class="text-sm text-red-600 mx-2 hidden">erros message</p>
                    </div>
                    <div class="flex flex-col w-4/12 my-2 ml-1">
                        <label for="ordering" class="text-gray-700 font-roboto pb-3">Ordering</label>
                        <input type="text" name="ordering" id="ordering" value="{{ old('ordering') ?? $course->ordering }}"
                            class="w-full border border-gray-400 px-2 py-2 rounded font-lato" placeholder="Ordering Course" readonly>
                        <p id="erorordering" class="text-sm text-red-600 mx-2 hidden">erros message</p>
                    </div>
                </div>
                <div class="flex flex-col w-full mt-5">
                    <label for="desc" class="text-gray-700 font-roboto pb-3">Descriptions</label>
                    <textarea name="description" id="desc" cols="30" rows="10" 
                        class="w-full border border-gray-400 rounded font-lato">{{ old('description') ?? $course->description }}</textarea>
                    <p id="#erorDesc" class="text-sm text-red-600 mx-2 hidden">erros message</p>
                </div>
                <div class="flex flex-col w-full mt-5">
                    <img id="imgView" src="{{ asset($course->image) }}" alt="not image" class="w-3/12">
                </div>
                <div class="flex flex-col w-full mt-5">
                    <label
                        class="w-64 flex flex-col items-center px-4 py-2 bg-white text-blue rounded-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-gray-200 hover:border hover:border-gray-500">
                        <span class="mt-2 text-base leading-normal text-gray-800">Select a image</span>
                        <input type="file" name="image" id="image" class="hidden" />
                    </label>
                    <p class="text-sm text-red-600 mx-2 hidden">erros message</p>
                </div>
                <div class="flex flex-row w-full mt-4">
                    <button type="submit" class="bg-green-800 text-white px-5 py-2 rounded">Submit</button>
                    <button type="reset" class="bg-red-800 text-white px-5 py-2 rounded mx-2">Reset</button>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection

@section('js')
@include('Admin.pages.courses.js')
<script>
    CKEDITOR.replace( 'description' );
</script>

@endsection