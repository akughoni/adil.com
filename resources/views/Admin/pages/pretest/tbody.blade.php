<div id="scroll" class="px-3 py-3 flex justify-center overflow-y-auto h-rem28">
  <table class="w-full h-auto text-sm bg-white shadow-md rounded mb-4 text-gray-800">
    <thead class="">
      <tr class="border-b">
        <th class="text-center py-3 px-3 w-1/12">
          <span class="text-xs">All</span><br>
          <input type="checkbox" onClick="select_all(this)" />
        </th>
        <th class="text-left p-3 px-5 w-8/12">Questions</th>
        <th class="text-left p-3 px-5 w-1/12">Section</th>
        <th class="text-center p-3 px-5 w-2/12">Actions</th>
      </tr>
    </thead>
    <tbody class="">
      @forelse($data as $key => $item)
    @php
        $no = $key +1;
    @endphp
    <tr class="border-b hover:bg-gray-100 text-gray-700" id="content">
      <td class="py-3 px-3 text-center">
        <input type="checkbox" name="select_delete" id="multiple" value="{{ $item->id }}"> 
      </td>
      <td class="p-3 px-5">{!! $item->question !!}</td>
      <td class="p-3 px-5">{{ $item->section->title ?? null }}</td>
      <td class="p-3 text-center px-5 flex justify-end">
        <a id="btn-edit" href="{{ route("pre-tests.edit", ["pre_test"=> $item->id ]) }}"
          class="mr-3 bg-orange-500 hover:bg-orange-700 text-white py-1 px-2 rounded">
          <span class="iconify text-lg" data-icon="feather:edit" data-inline="false"></span>
        </a>
        <button onclick="btn_del('{{ $item->id }}')"
          class="bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded">
          <span class="iconify text-lg" data-icon="ic:round-delete" data-inline="false"></span>
        </button>
      </td>
    </tr>
@empty
  <tr class="border-b hover:bg-gray-100 text-gray-700">
    <td colspan="5" class="p-5 px-5 text-center">Question Not Found</td>
  </tr>
@endforelse
    </tbody>
  </table>
</div>
<div class="w-full h-12 bg-white flex items-center">
  {{ $data->links('vendor.pagination.pagination') }}
</div>

