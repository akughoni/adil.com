<script src="{{ asset('js/all.js') }}"></script>

{{-- AJAX Index--}}
<script>
    var urlGetData = "{{ route('pre-tests.index') }}";
    var urlDeleteData = "{{ route('pre-tests.destroy') }}";
    var multipleDel = "{{ route('pre-tests.multiDelete') }}";
    var skip = 0;


    function getData(url = urlGetData) {
        var key = $('input#search').val();
        $.ajax({
            method: 'GET',
            url: url,
            data: {
                key: key
            },
            success: function (res) {                  
                $('.table').html(res);
            }
        });
    }

    $(document).on('click', '.pagination a', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        getData(url);
    });

    $('#search').keyup(function(){
        getData();
    })

    function multiple_del(){
        var id = [];
        
        $('#multiple:checked').each(function(index, data){
            id.push($(this).val());
        });

        if(id.length == 0){
            alertify.error('Silahkan Pilih yang mau dihapus!');
            getData();
        }

        var message = 'Apakah anda yakin mau menghapus data yang dipilih?';
        alertify.confirm("Delete Selected",message,
            function(){
                $.ajax({
                    type: "get",
                    url: multipleDel,
                    data: {
                        id:id
                    },
                    dataType: "JSON",
                    success: function (res) {
                        console.log(res);
                        if (res.status == 'success') {
                                        alertify.success('Berhasil Dihapus!');
                                    } else {
                                        alertify.error('Data Gagal Dihapus!');
                                    }
                        getData();                    
                    },
                });
            },
            function(){
                alertify.error('Cancel');
                getData();
            });
    }

    function btn_del(course_id) {
        var warning =
            "Apakah anda yakin ingin menghapus lecture ini?";
        alertify.confirm(warning,
            function () {
                $.ajax({
                    url: urlDeleteData,
                    method: 'GET',
                    dataType: 'JSON',
                    data: {
                        id: course_id
                    },
                    success: function (res) {
                        if (res.status == 'success') {
                            alertify.success('Course Dihapus!');
                        } else {
                            alertify.error('Course Gagal Dihapus!');
                        }
                    }
                }).done(function () {
                    getData();
                }).fail(function(e){
                    alert('fail');
                })
            },
            function () {
                alertify.error('Cancel');
            });
    }

    function select_all(select)
    {
        multiples = $('input[name="select_delete').select();
        for(var i = 0; i <= multiples.length; i++){
            multiples[i].checked = select.checked;
        }
    }
</script>

{{-- Created image preview--}}
<script>
    $(document).ready(function () {
        function getURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var filename = $("#inputFile").val();
                // filename = filename.substring(filename.lastIndexOf('\\') + 1);
                reader.onload = function (e) {
                    $('#imgView').attr('src', e.target.result);
                    $('#imgView').hide();
                    $('#imgView').fadeIn(700);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#image').change(function (e) {
            e.preventDefault();
            getURL(this);
        });

        function getFilename(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var filename = $("#file").val();
                $('#filename').append(filename);
            }
        }
        $('#file').change(function (e) {
            e.preventDefault();
            getFilename(this);
        });
    });

</script>

<script>
    var sidebar = document.getElementById('sidebar');

    function sidebarToggle() {
        if (sidebar.style.display === "none") {
            sidebar.style.display = "block";
        } else {
            sidebar.style.display = "none";
        }
    }

</script>