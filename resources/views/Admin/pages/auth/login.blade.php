<!doctype html>
<html lang="en">

<head>
  @include('Admin.includes.head')
  <title>Admin Login Page</title>
  <style>
    .login {
      background: url('/images/login-admin.jpeg');
    }
  </style>
</head>

<body class="h-screen font-sans login bg-cover">
  <div class="container mx-auto h-full flex flex-1 justify-center items-center">
    <div class="w-full max-w-lg">
      <div class="leading-loose">
        <form class="max-w-xl m-4 p-10 bg-white rounded shadow-xl" action="{{ route('AdminLoginProses') }}"
          method="POST" >
          @csrf
          <p class="text-gray-800 font-medium text-center text-lg font-bold">Admin Login Pages</p>
          <div class="">
            <label class="block text-sm text-gray-600" for="email">Email</label>
            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="email" name="email" type="text"
              required placeholder="Email@email.com" aria-label="email">
          </div>
          <div class="mt-2">
            <label class="block text-sm text-gray-600" for="password">Password</label>
            <input class="w-full px-5  py-1 text-gray-700 bg-gray-200 rounded" id="password" name="password"
              type="password" required placeholder="**********" aria-label="password">
          </div>
          <div class="mt-4 items-center justify-between">
            <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded"
              type="submit">Login</button>
            <a class="pl-5 inline-block right-0 align-baseline  font-bold text-sm text-500 hover:text-blue-800"
              href="#">
              Forgot Password?
            </a>
          </div>
        </form>

      </div>
    </div>
  </div>
</body>

</html>