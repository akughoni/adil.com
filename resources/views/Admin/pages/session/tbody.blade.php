<div id="scroll" class="px-3 py-3 flex justify-center overflow-y-auto h-rem28">
  <table class="w-full h-auto text-sm bg-white shadow-md rounded mb-4 text-gray-800">
    <thead class="">
      <tr class="border-b">
        <th class="text-center py-3 px-3 w-auto">
          <span class="text-xs">All</span><br>
          <input type="checkbox" onClick="select_all(this)" />
        </th>
        <th class="text-left p-3 px-5 w-auto">No.</th>
        <th class="text-left p-3 px-5 w-auto">Questions</th>
        <th class="text-left p-3 px-5 w-auto">Set Pretest</th>
        <th class="text-center p-3 px-5 w-2/12">Actions</th>
      </tr>
    </thead>
    <tbody class="">
      @forelse($data as $key => $item)
    @php
        $no = $key +1;
    @endphp
    <tr class="border-b hover:bg-gray-100 text-gray-700" id="content">
      <td class="py-3 px-3 text-center">
        <input type="checkbox" name="select_delete" id="multiple" value="{{ $item->id }}"> 
      </td>
      <td class="p-3 px-5">{{ $key+1 ?? null }}</td>
      <td class="p-3 px-5">{!! $item->question !!}</td>
      <td class="p-3 px-5">
          <div class="relative w-10">                
            <div id="line_toggle" class="w-full h-4 rounded-lg bg-gray-500 shadow-inner my-1"></div>
            <label onclick="setPretest('{{ $item->id }}')">
              <div id="{{ $item->id }}" class="w-6 h-6 rounded-full shadow-inner absolute top-0 -my-1 @if($item->set_pretest) bg-teal-600 right-0 @else bg-gray-200 left-0 @endif"></div>
            </label>
          </div>
      </td>
      <td class="p-3 text-center px-5 flex justify-end">
        <a id="btn-edit" href="{{ route("session-tests.edit", ["session_test"=> $item->id ]) }}"
          class="mr-3 bg-orange-500 hover:bg-orange-700 text-white py-1 px-2 rounded">
          <span class="iconify text-lg" data-icon="feather:edit" data-inline="false"></span>
        </a>
        <button onclick="btn_del('{{ $item->id }}')"
          class="bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded">
          <span class="iconify text-lg" data-icon="ic:round-delete" data-inline="false"></span>
        </button>
      </td>
    </tr>
@empty
  <tr class="border-b hover:bg-gray-100 text-gray-700">
    <td colspan="5" class="p-5 px-5 text-center">Session Questions Not Found</td>
  </tr>
@endforelse
    </tbody>
  </table>
</div>
<div class="w-full h-12 bg-white flex items-center">
  {{ $data->links('vendor.pagination.pagination') }}
</div>

