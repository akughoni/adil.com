@extends('Admin.layout.default')
@section('sidebar-sessions', 'bg-white')

@section('title','Admin Update Question')

@section('content')
<main class="bg-white-300 flex-1 mx-5 my-5 overflow-x-hidden">
    <div class="text-gray-900 bg-gray-100">
        <div class="mx-4 py-2 flex w-full">
            <h1 class="text-2xl mr-auto border-b border-gray-400 text-gray-700 pb-2 w-full">
                Add New Lecture
            </h1>
        </div>
        <div class="px-5 flex w-full">
            <form action="{{ route('session-tests.update', ['session_test' => $item->id]) }}" method="post" class="w-full" enctype="multipart/form-data"
                id="created">
                @method('PATCH')
                @csrf
                <input type="hidden" name="id" value="{{ $item->id }}">
                <div class="flex flex-row">
                    <div class="flex flex-col w-full mt-5 mr-2">
                        <label class="block tracking-wide text-gray-700 pb-3" for="grid-state">
                            Section
                        </label>
                    <div class="relative">
                        <select id="select_section" name="section_id" class="block border border-gray-400 appearance-none w-full bg-white border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option value="">---- Select Name Section ----</option>
                            @foreach ($data as $section)
                              <option value="{{ old('section_id') ?? $section->id }}" @if($item->section_id == $section->id) selected @endif>
                                  {{ $section->title }}
                              </option>
                            @endforeach
                          </select>
                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                        </div>
                    </div>
                    @error('section_id')
                    <div class="">
                        <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                    </div>
                    @enderror
                    </div>
                    <div class="flex flex-col w-full mt-5">
                    <label class="block tracking-wide text-gray-700 pb-3" for="grid-state">
                        Lecture
                    </label>
                    <div class="relative">
                        <select id="select_lecture" name="lecture_id" class="block border border-gray-400 appearance-none w-full bg-white border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            @foreach ($data as $lecture)
                              <option value="{{ old('lecture_id') ?? $lecture->id }}" @if($item->lecture_id == $lecture->id) selected @endif>
                                  {{ $lecture->title }}
                              </option>
                            @endforeach
                        </select>
                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                        </div>
                    </div>
                    @error('lecture_id')
                    <div class="">
                        <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                    </div>
                    @enderror
                    </div>
                </div>
                <div class="flex flex-col w-3/12">
                    <div class="flex mt-6">
                        <label class="flex items-center">
                            <input type="checkbox" class="form-checkbox" name="set_pretest" value="1" @if($item->set_pretest) checked @endif>
                            <span class="ml-2 text-gray-700">Set As Pretest Question</span></span>
                        </label>
                    </div>
                </div>
                <div class="flex flex-col w-full mt-5">
                    <label for="question" class="text-gray-700 font-roboto pb-3">Question</label>
                    <textarea name="question" id="question"  
                        class="w-full border border-gray-400 rounded font-lato ">{{ old('question') ?? $item->question }}</textarea>
                    @error('question')
                    <div class="">
                        <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                    </div>
                    @enderror
                </div>
                <div class="flex flex-row">
                    <div class="flex flex-col w-full mt-5 mr-1">
                        <label for="option" class="text-gray-700 font-roboto pb-3">Option 1</label>
                        <textarea name="answer_a" id="option"  
                            class="w-full border border-gray-400 rounded font-lato ">{{ old('answer_a') ?? $item->answer_a}}</textarea>
                        @error('answer_a')
                        <div class="">
                            <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                        </div>
                        @enderror
                    </div>
                    <div class="flex flex-col w-full mt-5 ml-1">
                        <label for="option" class="text-gray-700 font-roboto pb-3">Option 2</label>
                        <textarea name="answer_b" id="option"  
                            class="w-full border border-gray-400 rounded font-lato ">{{ old('answer_b') ?? $item->answer_b }}</textarea>
                        @error('answer_b')
                        <div class="">
                            <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="flex flex-row">
                    <div class="flex flex-col w-full mt-5 mr-1">
                        <label for="option" class="text-gray-700 font-roboto pb-3">Option 3</label>
                        <textarea name="answer_c" id="option"  
                            class="w-full border border-gray-400 rounded font-lato ">{{ old('answer_c') ?? $item->answer_c }}</textarea>
                        @error('answer_c')
                        <div class="">
                            <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                        </div>
                        @enderror
                    </div>
                    <div class="flex flex-col w-full mt-5 ml-1">
                        <label for="option" class="text-gray-700 font-roboto pb-3">Option 4</label>
                        <textarea name="answer_d" id="option"  
                            class="w-full border border-gray-400 rounded font-lato ">{{ old('answer_d') ?? $item->answer_d }}</textarea>
                        @error('answer_d')
                        <div class="">
                            <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="flex flex-col w-full mt-5">
                    <label class="block tracking-wide text-gray-700 pb-3" for="grid-state">
                        Select Option Right
                      </label>
                      <div class="relative">
                        <select name="right" class="block border border-gray-400 appearance-none w-full bg-white border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                          <option value="">---- Select Option Right ----</option>
                          <option value="A" @if($item->right == "A") selected @endif>A</option>
                          <option value="B" @if($item->right == "B") selected @endif>B</option>
                          <option value="C" @if($item->right == "C") selected @endif>C</option>
                          <option value="D" @if($item->right == "D") selected @endif>D</option>
                          
                        </select>
                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                        </div>
                      </div>
                    @error('right')
                    <div class="">
                        <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                    </div>
                    @enderror
                </div>
                <div class="flex flex-row w-full mt-6">
                    <button type="submit" class="bg-green-800 text-white px-5 py-2 rounded">Submit</button>
                    <button type="reset" class="bg-red-800 text-white px-5 py-2 rounded mx-2">Reset</button>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection

@section('js')
@include('Admin.pages.session.js')
<script>
    CKEDITOR.replace( 'question', {
        height: '8rem',
        customConfig: "{{ asset('ckeditor/config.js') }}"
    });
    option = ['a','b','c','d'];
    for(var i=0; i<= option.length; i++){
        var answer = 'answer_'+option[i];
        CKEDITOR.replace(answer, {
            height: '8rem'
        });
    }
</script>

@endsection