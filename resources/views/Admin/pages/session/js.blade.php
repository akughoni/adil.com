<script src="{{ asset('js/all.js') }}"></script>

{{-- AJAX Index--}}
<script>
    var urlGetData = "{{ route('session-tests.index') }}";
    var urlDeleteData = "{{ route('session-tests.destroy') }}";
    var multipleDel = "{{ route('session-tests.multiDelete') }}";
    var urlGetLecture = "{{ route('session-tests.get.lecture') }}";
    var skip = 0;

    $(document).ready(function () {
        setLecture();
    });
    function getData(url = urlGetData) {
        var key = $('input#search').val();
        $.ajax({
            method: 'GET',
            url: url,
            data: {
                key: key
            },
            success: function (res) {                  
                $('.table').html(res);
            }
        });
    }

    $(document).on('click', '.pagination a', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        getData(url);
    });

    $('#search').keyup(function(){
        getData();
    })

    function multiple_del(){
        var id = [];
        
        $('#multiple:checked').each(function(index, data){
            id.push($(this).val());
        });

        if(id.length == 0){
            alertify.error('Silahkan Pilih yang mau dihapus!');
            getData();
        }

        var message = 'Apakah anda yakin mau menghapus data yang dipilih?';
        alertify.confirm("Delete Selected",message,
            function(){
                $.ajax({
                    type: "get",
                    url: multipleDel,
                    data: {
                        id:id
                    },
                    dataType: "JSON",
                    success: function (res) {
                        console.log(res);
                        if (res.status == 'success') {
                            alertify.success('Berhasil Dihapus!');
                        } else {
                            alertify.error('Data Gagal Dihapus!');
                        }
                        getData();                    
                    },
                });
            },
            function(){
                alertify.error('Cancel');
                getData();
            });
    }

    function btn_del(id) {
        var warning =
            "Apakah anda yakin ingin menghapus pertanyaan ini?";
        alertify.confirm(warning,
            function () {
                $.ajax({
                    url: urlDeleteData,
                    method: 'GET',
                    dataType: 'JSON',
                    data: {
                        id: id
                    },
                    success: function (res) {
                        if (res.status == 'success') {
                            alertify.success('Question Dihapus!');
                        } else {
                            alertify.error('Question Gagal Dihapus!');
                        }
                    }
                }).done(function () {
                    getData();
                }).fail(function(e){
                    alert('fail');
                })
            },
            function () {
                alertify.error('Cancel');
            });
    }

    function select_all(select)
    {
        multiples = $('input[name="select_delete').select();
        for(var i = 0; i <= multiples.length; i++){
            multiples[i].checked = select.checked;
        }
    }

    function setLecture()
    {
        var id = $('#select_section').val();
        $.ajax({
            type: "GET",
            url: urlGetLecture,
            data: {
                id: id
            },
            dataType: "HTML",
            success: function (res) {
                console.log(res);
                $('#select_lecture').html(res);
            }
        });
    }
    // Select Lecture ID
    $('#select_section').change(function(){
        setLecture();
    });

    // set Pretest
    function setPretest(id)
    {
        $.ajax({
            type: "GET",
            url: "{{ route('session-tests.setPretest') }}",
            data: {
                id : id
            },
            dataType: "JSON",
        }).done(getData());
    }

</script>

{{-- Created image preview--}}
<script>
    $(document).ready(function () {
        function getURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var filename = $("#inputFile").val();
                // filename = filename.substring(filename.lastIndexOf('\\') + 1);
                reader.onload = function (e) {
                    $('#imgView').attr('src', e.target.result);
                    $('#imgView').hide();
                    $('#imgView').fadeIn(700);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#image').change(function (e) {
            e.preventDefault();
            getURL(this);
        });

        function getFilename(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var filename = $("#file").val();
                $('#filename').append(filename);
            }
        }
        $('#file').change(function (e) {
            e.preventDefault();
            getFilename(this);
        });
    });

</script>

<script>
    var sidebar = document.getElementById('sidebar');

    function sidebarToggle() {
        if (sidebar.style.display === "none") {
            sidebar.style.display = "block";
        } else {
            sidebar.style.display = "none";
        }
    }

</script>