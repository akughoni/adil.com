@extends('Admin.layout.default')

@section('title','Admin Dashboard')
@section('sidebar-user', 'bg-white')
@section('content')
<main class="bg-white-300 flex-1 p-3 overflow-hidden pb-10">
  <div class="text-gray-900 bg-gray-100">
    <div class="p-4 flex w-full">
      <h1 class="text-3xl mr-auto text-gray-700 w-full text-center uppercase">
        {{ $data->fullname }}
      </h1>
    </div>
    <hr>
    <div class="flex md:flex-row mt-4">
      <div
        class="h-32 flex-1 flex-row flex bg-white-1 border-1 border-gray-600 shadow-lg rounded-lg my-2 mx-1 sm:mx-3 xl:my-0 py-4 sm:py-8">
        <div class="flex flex-row w-3/5 h-full justify-end items-center sm:px-3">
          <span class="flex-1 iconify text-5rem text-gray-700" data-icon="ion:checkmark-done-circle-outline"
            data-inline="false"></span>
          <h1 class="text-gray-700 text-6xl">{{ (empty($data->pretest)) ? 0 : $section_pass->count() }} </h1>
        </div>
        <div class="flex flex-col w-2/5 h-full border-l-2 border-gray-600 justify-center">
          <p class="text-xl text-gray-700 font-lato font-semibold px-5">Courses</p>
          <p class="text-xl text-gray-700 font-lato font-semibold px-5">Pass</p>
        </div>
      </div>
      <div
        class="h-32 flex-1 flex-row flex bg-white-1 border-1 border-gray-600 shadow-lg rounded-lg my-2 mx-1 sm:mx-3 xl:my-0 py-4 sm:py-8 mx-0 xl:mx-2">
        <div class="flex flex-row w-3/6 h-full justify-end items-center px-3">
          <span class="flex-1 iconify text-5rem text-gray-700" data-icon="fe:bookmark" data-inline="false"></span>
          <h1 class="text-gray-700 text-6xl">{{ $data->sectionRecomend->count() }} </h1>
        </div>
        <div class="flex flex-col w-3/6 h-full border-l-2 border-gray-600 justify-center">
          <p class="text-xl text-gray-700 font-lato font-semibold px-5">Recomended</p>
          <p class="text-xl text-gray-700 font-lato font-semibold px-5">Courses</p>
        </div>
      </div>
      <div
        class="h-32 flex-1 flex-row flex bg-white-1 border-1 border-gray-600 shadow-lg rounded-lg my-2 mx-1 sm:mx-3 xl:my-0 py-4 sm:py-8">
        <div class="flex flex-row w-3/5 h-full justify-end items-center px-3">
          <span class="flex-1 iconify text-5rem text-gray-700" data-icon="ic-round-play-circle-outline"
            data-inline="false"></span>
          <h1 class="text-gray-700 text-6xl ">{{ $data->historySection->count() }}</h1>
        </div>
        <div class="flex flex-col w-2/5 h-full border-l-2 border-gray-600 justify-center">
          <p class="text-xl text-gray-700 font-lato font-semibold px-5">Courses</p>
          <p class="text-xl text-gray-700 font-lato font-semibold px-5">Taken</p>
        </div>
      </div>
    </div>
    {{-- jika sudah pretest --}}
    @if(!empty($data->pretest))
    <div class="flex flex-col sm:flex-row mt-4 bg-white-1 w-full h-auto shadow-md px-5 py-4 sm:py-6 sm:px-10">
      <div class="flex-1 h-full sm:mx-auto">
        <h1 class="font-medium font-lato text-lg text-gray-700 font-semibold">Courses Pass</h1>
        <div class="flex flex-col sm:px-2 mt-2">
          @forelse($section_pass as $pass)
          <a href="" class="cursor-default">
            <div class="w-full border shadow rounded border-gray-300 px-4 py-2 bg-white-1 hover:bg-white-2 hover:shadow-md">
              <h1 class="font-normal text-gray-700 font-lato text-lg">{{ $pass->title }} </h1>              
            </div>
          </a>
          @empty
            <p class="font-medium text-md text-left text-gray-600 mb-1">You have pass all course, you are the best.</p>
          @endforelse
        </div>
      </div>
      <div class="flex-1 h-full sm:mx-auto">
        <h1 class="font-medium font-lato text-lg text-gray-700 font-semibold">Recomended Courses</h1>
        <div class="flex flex-col sm:px-2 mt-2">
          @forelse($data->sections as $section)
          <a href="" class="cursor-default">
            <div class="w-full border shadow rounded border-gray-300 px-4 py-2 bg-white-1 hover:bg-white-2 hover:shadow-md">
              <h1 class="font-normal text-gray-700 font-lato text-lg">{{ $section->title }} </h1>              
            </div>
          </a>
          @empty
            <p class="font-medium text-md text-left text-gray-600 mb-1">You have pass all course, you are the best.</p>
          @endforelse
          @if($data->sectionRecomend->count() > 5 )
          <a href="{{ route('user.course.index') }} " class="py-3 rounded text-gray-700 hover:text-gray-700hover font-robot text-lg">Selengkapnya...</a>
          @endif
        </div>
      </div>
      <div class="flex-1 h-full sm:mx-auto">
        <h1 class="font-medium font-lato text-lg text-gray-700 font-semibold">Last Courses</h1>
        <div class="flex flex-col sm:px-2 mt-2">
          @forelse($data->historySection->sortByDesc('historySection.updated_at') as $history)
          <a href="{{ route('user.course.show', ['id' => $history->id ]) }} " class="cursor-default">
            <div class="w-full border shadow rounded border-gray-300 px-4 py-2 bg-white-1 hover:bg-white-2 hover:shadow-md">
              <h1 class="font-normal text-gray-700hover font-lato text-lg">{{ $history->section->title }} </h1>
              <p class="font-normal font-openSans text-sm text-gray-600">{{ $history->updated_at->diffForHumans() }} </p>
            </div>
          </a>
          @empty
            <p class="font-medium text-md text-left text-gray-600 mb-1">You haven't taken a course. let's take it!</p>
          @endforelse
        </div>
      </div>
    </div>
    @else
    {{-- jika belum pretest --}}
    <div class="flex flex-col justify-center items-center mt-4 bg-white-1 w-full h-auto md:h-bg20rem shadow-md py-6 px-16 sm:px-24 relative">
      <div class="w-full font-openSans text-center text-gray-800">
        <strong>{{ $data->fullname }}</strong>
        <p class="text-md">belum mengambil Pretest.</p>
      </div>
    </div>
    @endif
  </div>
</main>
@endsection

@section('js')
@include('Admin.pages.user.js')
@endsection