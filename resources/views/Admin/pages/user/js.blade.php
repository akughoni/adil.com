<script src="{{ asset('js/all.js') }}"></script>

<script>
    let resetUserUrl = "{{ route('AdminUserReset') }} "
	$(document).ready(function(){
        // getData();
    });
    function getData(url = "{{ route('AdminUser') }}"){
        var key = $('input#search').val();
        $.ajax({
            type: "GET",
            url: url,
            data: {
                key: key
            },
            success: function (res) {
                $('.table').html(res);
            }
        })
    }
    $('#search').keyup(function(){
        getData();
    })

    $(document).on('click', '.pagination a', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        getData(url);
    });

    $('.reset').click(function(){
        let id = $(this).attr('data');
        alertify.confirm('<p class="text-gray-700 text-md">Confirm Reset User</p>', '<p class="text-gray-700 text-md">Apakah anda yakin ingin mereset akun user ini?</p>',
        function(){
            $.ajax({
                type: "POST",
                url: resetUserUrl,
                data: {
                    _token: "{{ csrf_token() }}",
                    id:id
                },
                dataType: "JSON",
                success: function (res) {
                    alertify.success(res.message);
                }
            });
        }
        ,function(){
            alertify.error('Cancel Reset')
        });
    })
    
</script>
<script>
	var sidebar = document.getElementById('sidebar');
  
  function sidebarToggle() {
      if (sidebar.style.display === "none") {
          sidebar.style.display = "block";
      } else {
          sidebar.style.display = "none";
      }
  }
  
  var profileDropdown = document.getElementById('ProfileDropDown');
  
  function profileToggle() {
      if (profileDropdown.style.display === "none") {
          profileDropdown.style.display = "block";
      } else {
          profileDropdown.style.display = "none";
      }
  }
  
  
  /**
   * ### Modals ###
   */
  
  function toggleModal(action, elem_trigger)
  {
      elem_trigger.addEventListener('click', function () {
          if (action == 'add') {
              let modal_id = this.dataset.modal;
              document.getElementById(`${modal_id}`).classList.add('modal-is-open');
          } else {
              // Automaticlly get the opned modal ID
              let modal_id = elem_trigger.closest('.modal-wrapper').getAttribute('id');
              document.getElementById(`${modal_id}`).classList.remove('modal-is-open');
          }
      });
  }
  
  
  // Check if there is modals on the page
  if (document.querySelector('.modal-wrapper'))
  {
      // Open the modal
      document.querySelectorAll('.modal-trigger').forEach(btn => {
          toggleModal('add', btn);
      });
      
      // close the modal
      document.querySelectorAll('.close-modal').forEach(btn => {
          toggleModal('remove', btn);
      });
  }

  
</script>