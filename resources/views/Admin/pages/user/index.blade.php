@extends('Admin.layout.default')

@section('title','Admin Dashboard')
@section('sidebar-user', 'bg-white')
@section('content')
<main class="bg-white-300 flex-1 px-3 py-1 overflow-hidden">
  <div class="text-gray-900 bg-gray-100">
    <div class="p-4 flex w-full">
      <h1 class="text-3xl mr-auto text-gray-700">
        Users
      </h1>
      <a class="px-5 py-2 bg-teal-700 text-white-1 rounded" href="{{ route('getSessionResutl') }} ">Export Score</a>
      <div class="mr-0">
        <label for="search" class="px-5 text-gray-700">Serach </label>
        <input type="text" name="search" id="search" class="px-2 py-2 border-gray-400 border rounded">
      </div>
    </div>
    <hr>
    <div class="table">
      @include('Admin.pages.user.tbody')
    </div>
  </div>
</main>
@endsection

@section('js')
@include('Admin.pages.user.js')
@endsection