<div class="px-3 py-3 flex justify-center">
  <table class="w-full text-sm bg-white shadow-md rounded mb-4 text-gray-800">
    <thead>
      <tr class="border-b">
        <th class="text-left p-3 px-5 w-1/12">No.</th>
        <th class="text-left p-3 px-5 w-1/12">nama</th>
        <th class="text-left p-3 px-5 w-5/12">Email</th>
        <th class="text-center p-3 px-5 w-1/12">Actions</th>
      </tr>
    </thead>
    <tbody>
    @if($data->count() > 0)
      @foreach ($data as $key => $item)
      <tr class="border-b hover:bg-gray-100 text-gray-700">
        <td class="p-3 px-5">{{ $key+1 }} </td>
        <td class="p-3 px-5">{{ $item->fullname }}</td>
        <td class="p-3 px-5">{{ $item->email }}</td>
        <td class="p-3 px-5 flex justify-end">
          <a href="{{ route('AdminShowUser',['id' => $item->id]) }}" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Show</a>
          <button type="button" data="{{ $item->id }}" class="reset text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Reset</button>
        </td>
      </tr>
      @endforeach
    @else
      <tr class="border-b hover:bg-gray-100 text-gray-700">
        <td colspan="4" class="p-3 px-5 text-center">User Not Found</td>
      </tr>
    @endif
    </tbody>
  </table>
</div>
<div class="w-full h-16 bg-white flex items-center">
  {{ $data->links('vendor.pagination.pagination') }}
</div>