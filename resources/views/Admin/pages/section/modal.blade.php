<div id='centeredFormModal' class="modal-wrapper">
  <div class="overlay close-modal"></div>
  <div class="modal modal-centered">
    <div class="modal-content shadow-lg p-5">
      <div class="border-b p-2 pb-3 pt-0 mb-2">
        <div class="flex justify-between items-center">
          Import From Excel
          <span class='close-modal cursor-pointer px-3 py-1 rounded-full bg-gray-100 hover:bg-gray-200'>
            <i class="fas fa-times text-gray-700"></i>
          </span>
        </div>
      </div>
      <!-- Modal content -->
      <form class="w-full h-64 flex flex-col justify-center" method="POST" action="{{ route('AdminSectionImport') }}"
        enctype="multipart/form-data">
        <div class="flex flex-col w-full justify-center items-center bg-gray-200 h-64">
          <label class="text-sm text-gray-800">Contoh Format, File Sebaiknya .xls</label>
          <img src="{{ asset('images/courses/contoh-xls.png') }}" alt="Contoh Format File">
          <p class="text-gray-700" id="filename"></p>
          <label
            class="w-64 flex flex-col items-center px-4 py-2 bg-white text-blue rounded-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-gray-200 hover:border hover:border-gray-500">
            <span class="mt-2 text-base leading-normal text-gray-800">Select a File</span>
            <input type="file" name="file" id="file" class="hidden" />
          </label>
        </div>
        @csrf
        <div class="flex flex-row w-full justify-center mt-5">
          <button type="submit" class="bg-green-800 text-white px-10 py-2 rounded">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>