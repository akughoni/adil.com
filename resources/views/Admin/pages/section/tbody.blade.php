<div class="px-3 py-3 flex justify-center overflow-y-auto h-rem28" >
  <table class="w-full text-sm bg-white shadow-md rounded mb-4 text-gray-800">
    <thead>
      <tr class="border-b">
        <th class="text-center py-3 px-3 w-auto">
          <span class="text-xs">All</span><br>
          <input type="checkbox" onClick="select_all(this)" />
        </th>
        <th class="text-left p-3 px-5 w-1/12">No.</th>
        <th class="text-left p-3 px-5 w-1/12">ID</th>
        <th class="text-left p-3 px-5 w-5/12">Title</th>
        <th class="text-left p-3 px-5 w-2/12">Course</th>
        <th class="text-center p-3 px-5 w-3/12">Amount of Lecture</th>
        <th class="text-center p-3 px-5 w-1/12">Actions</th>
      </tr>
    </thead>
    <tbody>
    @if($data->count() > 0)
      @foreach ($data as $key => $section)
        <tr class="border-b hover:bg-gray-100 text-gray-700" id="content">
          <td class="py-3 px-3 text-center">
            <input type="checkbox" name="select_delete" id="multiple" value="{{ $section->id }}"> 
          </td>
          <td class="p-3 px-5">{{ $section->ordering }}</td>
          <td class="p-3 px-5">{{ $section->id }}</td>
          <td class="p-3 px-5">{{ $section->title }}</td>
          <td class="p-3 px-5">{{ $section->course->title }}</td>
          <td class="p-3 text-center px-5">{{ $section->lectures->count() }}</td>
          <td class="p-3 text-center px-5 flex justify-end">
            <a id="btn-edit" href="{{ route("AdminSectionEdit", ["id"=> $section->id ]) }}"
              class="mr-3 text-sm bg-orange-500 hover:bg-orange-700 text-white py-1 px-2 rounded">
              <span class="iconify text-lg" data-icon="feather:edit" data-inline="false"></span>
            </a>
            <button onclick="btn_del('{{ $section->id }}')"
              class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded">
              <span class="iconify text-lg" data-icon="ic:round-delete" data-inline="false"></span>
            </button>
          </td>
        </tr>
      @endforeach
    @else
      <tr class="border-b hover:bg-gray-100 text-gray-700">
        <td colspan="6" class="p-5 px-5 text-center">Sections Not Found</td>
      </tr>
    @endif
    </tbody>
  </table>
</div>
<div class="w-full h-16 bg-white flex items-center">
  {{ $data->links('vendor.pagination.pagination') }}
</div>