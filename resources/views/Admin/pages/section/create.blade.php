@extends('Admin.layout.default')
@section('sidebar-sections', 'bg-white')

@section('title','Admin Create Section')

@section('content')
<main class="bg-white-300 flex-1 mx-5 my-5 overflow-x-hidden">
    <div class="text-gray-900 bg-gray-100">
        <div class="mx-4 py-2 flex w-full">
            <h1 class="text-2xl mr-auto border-b border-gray-400 text-gray-700 pb-2 w-full">
                Add New Section
            </h1>
        </div>
        <div class="px-5 flex w-full">
            <form action="{{ route('AdminSectionStore') }}" method="post" class="w-full" enctype="multipart/form-data"
                id="created">
                @csrf
                <div class="flex flex-row">
                    <div class="flex flex-col w-8/12 my-2 mr-1">
                        <label for="title" class="text-gray-700 font-roboto pb-3">Title</label>
                        <input type="text" name="title" id="title" value="{{ old('title') }}"
                            class="w-full border border-gray-400 px-2 py-2 rounded font-lato" placeholder="Title">
                        @error('title')
                        <div>
                            <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                        </div>
                        @enderror
                    </div>
                    <div class="flex flex-col w-4/12 my-2 mr-1">
                        <label for="ordering" class="text-gray-700 font-roboto pb-3">Ordering</label>
                        <input type="text" name="ordering" id="ordering" value="{{ old('ordering') ?? $count+1 }}"
                            class="w-full bg-gray-300 border border-gray-400 px-2 py-2 rounded font-lato" placeholder="ordering" readonly>
                        @error('ordering')
                        <div>
                            <p id="erorordering" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="flex flex-col w-full mt-5">
                    <label class="block tracking-wide text-gray-700 pb-3" for="grid-state">
                        Course
                      </label>
                      <div class="relative">
                        <select name="course_id" class="block border border-gray-400 appearance-none w-full bg-white border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                          <option value="">---- Select Name Course ----</option>
                          @foreach ($courses as $item)
                            <option value="{{ $item->id }}">{{ $item->title }}</option>
                          @endforeach
                        </select>
                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                        </div>
                      </div>
                    @error('course_id')
                    <div class="">
                        <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                    </div>
                    @enderror
                </div>
                <div class="flex flex-col w-full mt-5">
                    <label for="desc" class="text-gray-700 font-roboto pb-3">Descriptions</label>
                    <textarea name="description" id="desc" cols="30" rows="10" 
                        class="w-full border border-gray-400 rounded font-lato">{{ old('description') }}</textarea>
                    @error('description')
                    <div class="">
                        <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                    </div>
                    @enderror
                </div>
                <div class="flex flex-col w-full mt-5">
                    <img id="imgView" src="{{ asset('images/no_image.png') }}" alt="not image" class="w-3/12">
                    @error('image')
                    <p id="erorTitle" class="text-sm text-red-600 mx-2 py-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="flex flex-col w-full mt-5">
                    <label
                        class="w-64 flex flex-col items-center px-4 py-2 bg-white text-blue rounded-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-gray-200 hover:border hover:border-gray-500">
                        <span class="mt-2 text-base leading-normal text-gray-800">Select a image</span>
                        <input type="file" name="image" id="image" class="hidden" />
                    </label>
                    <p class="text-sm text-red-600 mx-2 hidden">erros message</p>
                </div>
                <div class="flex flex-row w-full mt-4">
                    <button type="submit" class="bg-green-800 text-white px-5 py-2 rounded">Submit</button>
                    <button type="reset" class="bg-red-800 text-white px-5 py-2 rounded mx-2">Reset</button>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection

@section('js')
@include('Admin.pages.section.js')
<script>
    CKEDITOR.replace( 'description' );
</script>

@endsection