@extends('Admin.layout.default')

@section('title','Admin Dashboard')
@section('sidebar-dashboard', 'bg-white')

@section('content')
<main class="bg-white-300 flex-1 p-3 overflow-hidden">

</main>
@endsection

@section('js')
  @include('Admin.pages.dashboard.js')
@endsection