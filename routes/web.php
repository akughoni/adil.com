<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Routing\RouteGroup;

// User route
Route::get('/', function () {
    return view('User.pages.landing.index');
})->name('landing');
Route::get('/login', 'Auth\AuthController@index')->middleware('guest')->name('auth.login');
Route::get('/logout', 'Auth\AuthController@logout')->name('auth.logout');
Route::post('/postLogin', 'Auth\AuthController@postLogin')->middleware('guest')->name('auth.postLogin');
Route::get('/registration', 'Auth\AuthController@registration')->middleware('guest')->name('auth.registration');
Route::get('/user/verify/{token}', 'Auth\AuthController@verify')->middleware('guest')->name('auth.verify');
Route::post('/registration', 'Auth\AuthController@postRegistration')->middleware('guest')->name('auth.postRegistration');
Route::get('/reset-password', 'Auth\AuthController@resetPassword')->middleware('guest')->name('auth.resetPassword');
Route::post('/reset-password/send', 'Auth\AuthController@sendResetMail')->middleware('guest')->name('auth.sendResetMail');
Route::get('/reset-password/new-password/{id}', 'Auth\AuthController@newPassword')->middleware('guest')->name('auth.newPassword');
Route::post('/reset-password/store', 'Auth\AuthController@storeNewPassword')->middleware('guest')->name('auth.store.newPassword');
Route::get('/resend', 'Auth\AuthController@resend')->middleware('guest')->name('auth.resend');
Route::get('/help', 'Auth\AuthController@help')->middleware('guest')->name('auth.help');
Route::post('/resend/store', 'Auth\AuthController@resendStore')->middleware('guest')->name('auth.resend.store');

Route::group(['middleware' => ['authCheck:user']], function () {
    Route::get('/dashboard', 'User\DashboardController@index')->name('user.dashboard');
    Route::get('/pretest/start', 'User\PretestController@index')->name('user.pretest');
    Route::get('/pretest', 'User\PretestController@landing')->name('user.before.pretest');
    Route::post('/pretest', 'User\PretestController@submit')->name('user.pretest.submit');
    Route::get('/pretest/result', 'User\PretestController@set_section_recomend');

    // Courses
    Route::get('/courses', 'User\CourseController@index')->name('user.course.index');
    Route::get('/courses/{id}/show', 'User\CourseController@show')->name('user.course.show');
    Route::get('/courses/{section}/video', 'User\CourseController@video')->name('user.course.video');

    
    // All Courses
    Route::get('/courses/all', 'User\AllCourseController@index')->name('user.course.all');
    Route::get('/courses/all/{id}/show', 'User\AllCourseController@show')->name('user.course.all.show');
    Route::get('/courses/all/{section}/video', 'User\AllCourseController@video')->name('user.course.all.video');


    //sessiom TEST
    Route::get('/session/{section}/test', 'User\SessionController@index')->name('user.session.start');
    Route::post('/session', 'User\SessionController@submit')->name('user.session.submit');
    Route::get('/session/report', 'User\SessionController@report')->name('user.session.report');

    // profile
    Route::get('/profile/change-password', 'User\ProfileController@changePassword')->name('profile.change');
    Route::Post('/profile/change-password/store', 'User\ProfileController@updatePassword')->name('profile.update.password');
    Route::get('/profile/basic', 'User\ProfileController@basic')->name('profile.basic');
    Route::resource('/profile', 'User\ProfileController')->except(['destroy, create, edit, update']);
});



// Admin Route
Route::group(['prefix' => 'admin'], function () {
    // Auth
    Route::get('/login', 'Admin\AdminController@login')->name('AdminLogin')->middleware('guest');
    Route::post('/loginProses', 'Admin\AdminController@loginProses')->name('AdminLoginProses')->middleware('guest');
    Route::get('/logout', 'Admin\AdminController@logout')->name('AdminLogout');

    Route::group(['middleware' => ['authCheck:admin']], function () {
        // Dashboard
        Route::get('/', 'Admin\AdminDashboard@index')->name('AdminDashboard');
        // User
        Route::get('/user', 'Admin\AdminUser@index')->name('AdminUser');
        Route::get('/getDataUser', 'Admin\AdminUser@getData')->name('getUserData');
        Route::get('/export-score', 'Admin\AdminUser@exportScore')->name('getSessionResutl');
        Route::post('/user/reset', 'Admin\AdminUser@userReset')->name('AdminUserReset');
        Route::get('/user/{id}/show', 'Admin\AdminUser@show')->name('AdminShowUser');

        // Courses
        Route::get('/courses', 'Admin\CourseController@index')->name('AdminCourses');
        Route::get('/courses/add', 'Admin\CourseController@create')->name('AdminCourseAdd');
        Route::get('/courses/edit/{id}', 'Admin\CourseController@edit')->name('AdminCourseEdit')->where('name', '[A-Za-z0-9]+');
        Route::patch('/courses/update', 'Admin\CourseController@update')->name('AdminCourseUpdate');
        Route::get('/courses/delete', 'Admin\CourseController@destroy')->name('AdminCourseDel');
        Route::get('/courses/multiDelete', 'Admin\CourseController@multiDelete')->name('AdminCourseMultiDel');
        Route::get('/courses/getDataCourses', 'Admin\CourseController@getDataCourses')->name('getDataCourses');
        Route::post('/courses/add', 'Admin\CourseController@store')->name('AdminCourseStore');
        Route::post('/courses/import', 'Admin\CourseController@import')->name('AdminCourseImport');
        Route::get('/courses/{id}', 'Admin\SectionController@show')->name('AdminShowSection');
        Route::get('/courses/active/{id}', 'Admin\CourseController@active')->name('AdminCourseActive');

        // Sections
        Route::get('/sections', 'Admin\SectionController@index')->name('AdminSections');
        Route::get('/sections/getData', 'Admin\SectionController@getData')->name('getDataSections');
        Route::get('/sections/add', 'Admin\SectionController@create')->name('AdminSectionAdd');
        Route::post('/sections/add', 'Admin\SectionController@store')->name('AdminSectionStore');
        Route::get('/sections/edit/{id}', 'Admin\SectionController@edit')->name('AdminSectionEdit')->where('name', '[A-Za-z0-9]+');
        Route::patch('/sections/update', 'Admin\SectionController@update')->name('AdminSectionUpdate');
        Route::get('/sections/delete', 'Admin\SectionController@destroy')->name('AdminSectionDel');
        Route::get('/sections/multiDelete', 'Admin\SectionController@multiDelete')->name('AdminSectionMultiDel');
        Route::post('/sections/import', 'Admin\SectionController@import')->name('AdminSectionImport');

        // Lecture
        Route::get('/lecture', 'Admin\LectureController@index')->name('AdminLecture');
        // Route::get('/lecture/getData', 'Admin\LectureController@getData')->name('getDataLecture');
        Route::get('/lecture/add', 'Admin\LectureController@create')->name('AdminLectureAdd');
        Route::post('/lecture/add', 'Admin\LectureController@store')->name('AdminLectureStore');
        Route::get('/lecture/edit/{id}', 'Admin\LectureController@edit')->name('AdminLectureEdit')->where('name', '[A-Za-z0-9]+');
        Route::patch('/lecture/update', 'Admin\LectureController@update')->name('AdminLectureUpdate');
        Route::get('/lecture/delete', 'Admin\LectureController@destroy')->name('AdminLectureDel');
        Route::get('/lecture/multiDelete', 'Admin\LectureController@multiDelete')->name('AdminLectureMultiDel');
        Route::post('/lecture/import', 'Admin\LectureController@import')->name('AdminLectureImport');

        // pretest (jika tambah route maka sebelum resaour)
        Route::get('/pre-tests/destroy', 'Admin\PretestController@destroy')->name('pre-tests.destroy');
        Route::get('/pre-tests/multiDelete', 'Admin\PretestController@multiDelete')->name('pre-tests.multiDelete');
        Route::post('/pre-tests/import', 'Admin\PretestController@import')->name('pre-tests.import');
        Route::resource('pre-tests', 'Admin\PretestController')->except([
            'destroy'
        ]);

        // Session Test
        Route::get('/session-tests/lecture', 'Admin\SessionController@getLecture')->name('session-tests.get.lecture');
        Route::get('/session-tests/destroy', 'Admin\SessionController@destroy')->name('session-tests.destroy');
        Route::get('/session-tests/multiDelete', 'Admin\SessionController@multiDelete')->name('session-tests.multiDelete');
        Route::post('/session-tests/import', 'Admin\SessionController@import')->name('session-tests.import');
        Route::get('/sessesion-tests/set-pretest', 'Admin\SessionController@setPretest')->name('session-tests.setPretest');
        Route::resource('session-tests', 'Admin\SessionController')->except([
            'destroy'
        ]);
    });
});
